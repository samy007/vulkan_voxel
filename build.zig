const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const exe = b.addExecutable(.{
        .name = "vulkan",
        .root_source_file = b.path("src/main.zig"),
        .target = target,
        .optimize = optimize,
    });

    exe.addIncludePath(.{
        .cwd_relative = "."
    });
    exe.addSystemIncludePath(.{
        .cwd_relative = "/usr/include"
    });
    exe.addIncludePath(.{
        .cwd_relative = "/home/samy/github/cglm/include/"
    });
    exe.addLibraryPath(.{
        .cwd_relative = "/home/samy/github/cglm/build"
    });


    exe.linkSystemLibrary2("cglm", .{ .preferred_link_mode = .static });
    exe.linkSystemLibrary("glfw3");
    exe.linkSystemLibrary("vulkan");
    exe.linkSystemLibrary("assimp");
    exe.linkSystemLibrary("tracy");

    // C / C++ bindings
    const CFlags = &[_][]const u8{"-fPIC"};
    exe.addCSourceFile(.{.file = .{ .cwd_relative = "bindings/stb_image.c" }, .flags = CFlags });
    exe.addCSourceFile(.{.file = .{ .cwd_relative = "bindings/bindings.cpp" } });
    exe.addCSourceFile(.{.file = .{ .cwd_relative = "bindings/vk_mem_alloc.cpp" } });

    // c++ is required for bindings
    exe.linkLibC();
    exe.linkLibCpp();




    b.installArtifact(exe);
    const run_cmd = b.addRunArtifact(exe);
    run_cmd.step.dependOn(b.getInstallStep());

    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    const lib_unit_tests = b.addTest(.{
        .root_source_file = b.path("src/root.zig"),
        .target = target,
        .optimize = optimize,
    });

    const run_lib_unit_tests = b.addRunArtifact(lib_unit_tests);

    const exe_unit_tests = b.addTest(.{
        .root_source_file = b.path("src/main.zig"),
        .target = target,
        .optimize = optimize,
    });

    const run_exe_unit_tests = b.addRunArtifact(exe_unit_tests);

    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_lib_unit_tests.step);
    test_step.dependOn(&run_exe_unit_tests.step);
}

const std = @import("std");
const c = @import("c.zig");

const Camera = @This();

const Vec3 = c.vec3;
const Mat4 = c.mat4;

const MAT4_IDENTITY = c.mat4 {
    c.vec4 {1.0, 0.0, 0.0, 0.0},
    c.vec4 {0.0, 1.0, 0.0, 0.0},
    c.vec4 {0.0, 0.0, 1.0, 0.0},
    c.vec4 {0.0, 0.0, 0.0, 1.0}
};

perspective: Mat4 align(64),
view: Mat4 align(64),


yaw: f32,
pitch: f32,
roll: f32,

position: Vec3 align(32),

fov: f32,
nearz: f32,
farz: f32,

pub const rotation_speed = 1;
pub const translation_speed = 5;
pub const WORLD_UP = Vec3 { 0.0, 0.0, 1.0 };

fn _ptr(v: *const [3]f32) [*c]f32 {
    return @ptrCast(@constCast(v));
}

pub fn init(fov: f32, aspect: f32, nearz: f32, farz: f32) Camera {
    var camera = Camera {
        .position = Vec3 { 0.0, 0.0, 0.0 },
        .yaw = 0,
        .pitch = 0,
        .roll = 0,
        .fov = 0,
        .nearz = 0,
        .farz = 0,
        .view = MAT4_IDENTITY,
        .perspective = MAT4_IDENTITY
    };
    camera.update_matrices();
    camera.set_perspective(fov, aspect, nearz, farz);
    return camera;
}

fn update_matrices(self: *Camera) void {
    var direction = self.get_front();
    var lookat: Vec3 align(32) = undefined;
    var up: Vec3 align(32) = WORLD_UP;

    c.glmc_vec3_add(&self.position, &direction, &lookat);
    c.glmc_vec3_rotate(&up, self.roll, _ptr(&Vec3 { 1.0, 0.0, 0.0 }));

    c.glmc_lookat(&self.position, &lookat, &up, &self.view);
}

fn mod_rot(self: *Camera) void {
    self.pitch = std.math.clamp(self.pitch, -std.math.pi / 2.0 + 0.01, std.math.pi / 2.0 - 0.01);
}

pub fn rotate(self: *Camera, yaw: f32, pitch: f32, roll: f32) void {
    self.yaw += yaw;
    self.pitch += pitch;
    self.roll += roll;
    self.mod_rot();
    self.update_matrices();
}

pub fn translate(self: *Camera, translation: Vec3) void {
    c.glmc_vec3_add(&self.position, _ptr(&translation), &self.position);
    self.update_matrices();
}

pub fn set_rotation(self: *Camera, yaw: f32, pitch: f32, roll: f32) void {
    self.yaw = yaw;
    self.pitch = pitch;
    self.roll = roll;
    self.mod_rot();
    self.update_matrices();
}


pub fn set_position(self: *Camera, position: Vec3) void {
    self.position = position;
    self.update_matrices();
}

pub fn set_perspective(self: *Camera, fov: f32, aspect: f32, nearz: f32, farz: f32) void {
    self.fov = fov;
    self.nearz = nearz;
    self.farz = farz;
    c.glmc_perspective(fov, aspect, nearz, farz, &self.perspective);
    self.perspective[1][1] *= -1;
}

pub fn update_aspect_ratio(self: *Camera, aspect: f32) void {
    c.glmc_perspective(self.fov, aspect, self.nearz, self.farz, &self.perspective);
    self.perspective[1][1] *= -1;
}

pub fn get_front(self: Camera) Vec3 {
    return Vec3 {
        @cos(self.yaw) * @cos(self.pitch),
        @sin(self.yaw) * @cos(self.pitch),
        @sin(self.pitch),
    };
}

pub fn get_back(self: Camera) Vec3 {
    return Vec3 {
        - @cos(self.yaw) * @cos(self.pitch),
        - @sin(self.yaw) * @cos(self.pitch),
        - @sin(self.pitch),
    };
}

pub fn get_right(self: Camera) Vec3 {
    var front align(32) = self.get_front();
    var right: Vec3 align(32) = undefined;
    var up align(32) = WORLD_UP;
    c.glmc_vec3_crossn(&front, &up, &right);
    return right;
}

pub fn get_left(self: Camera) Vec3 {
    var right = self.get_right();
    c.glmc_vec3_negate(&right);
    return right;
}

const std = @import("std");
const c = @import("c.zig");
const tracy = @import("tracy.zig");
const utils = @import("utils.zig");

const logger = utils.Logger(.VulkanEngine);
const check = utils.Scoped(logger).check;

// TODO: export references
pub const Allocation     = @import("VulkanEngine/Allocation.zig");
pub const Buffer         = @import("VulkanEngine/Buffer.zig");
pub const Device         = @import("VulkanEngine/Device.zig");
pub const GlfwWindow     = @import("VulkanEngine/GlfwWindow.zig");
pub const Image          = @import("VulkanEngine/Image.zig");
pub const Instance       = @import("VulkanEngine/Instance.zig");
pub const PhysicalDevice = @import("VulkanEngine/PhysicalDevice.zig");
pub const SwapChain      = @import("VulkanEngine/SwapChain.zig");

const VulkanEngine = @This();

pub const api_allocator = null;
//&.{
//    .pfnAllocation = PFN_vkAllocationFunction,
//    .pfnFree = PFN_vkFreeFunction,
//    .pfnReallocation = PFN_vkReallocationFunction
//};
pub var vma_allocator: c.VmaAllocator = undefined;


pub const VulkanError = error {
    NotReady,
    Timeout,
    EventSet,
    EventReset,
    Incomplete,
    OutOfHostMemory,
    OutOfDeviceMemory,
    InitializationFailed,
    DeviceLost,
    MemoryMapFailed,
    LayerNotPresent,
    ExtensionNotPresent,
    FeatureNotPresent,
    IncompatibleDriver,
    TooManyObjects,
    FormatNotSupported,
    SurfaceLostKhr,
    NativeWindowInUseKhr,
    SuboptimalKhr,
    OutOfDateKhr,
    IncompatibleDisplayKhr,
    ValidationFailedExt,
    InvalidShaderNv,
    UnknownError
};

pub fn emit_error(code: i32) VulkanError!void {
    if(code == c.VK_SUCCESS) 
        return; 

    return switch(code) {
        c.VK_NOT_READY => VulkanError.NotReady,
        c.VK_TIMEOUT => VulkanError.Timeout,
        c.VK_EVENT_SET => VulkanError.EventSet,
        c.VK_EVENT_RESET => VulkanError.EventReset,
        c.VK_INCOMPLETE => VulkanError.Incomplete,
        c.VK_ERROR_OUT_OF_HOST_MEMORY => VulkanError.OutOfHostMemory,
        c.VK_ERROR_OUT_OF_DEVICE_MEMORY => VulkanError.OutOfDeviceMemory,
        c.VK_ERROR_INITIALIZATION_FAILED => VulkanError.InitializationFailed,
        c.VK_ERROR_DEVICE_LOST => VulkanError.DeviceLost,
        c.VK_ERROR_MEMORY_MAP_FAILED => VulkanError.MemoryMapFailed,
        c.VK_ERROR_LAYER_NOT_PRESENT => VulkanError.LayerNotPresent,
        c.VK_ERROR_EXTENSION_NOT_PRESENT => VulkanError.ExtensionNotPresent,
        c.VK_ERROR_FEATURE_NOT_PRESENT => VulkanError.FeatureNotPresent,
        c.VK_ERROR_INCOMPATIBLE_DRIVER => VulkanError.IncompatibleDriver,
        c.VK_ERROR_TOO_MANY_OBJECTS => VulkanError.TooManyObjects,
        c.VK_ERROR_FORMAT_NOT_SUPPORTED => VulkanError.FormatNotSupported,
        c.VK_ERROR_SURFACE_LOST_KHR => VulkanError.SurfaceLostKhr,
        c.VK_ERROR_NATIVE_WINDOW_IN_USE_KHR => VulkanError.NativeWindowInUseKhr,
        c.VK_SUBOPTIMAL_KHR => VulkanError.SuboptimalKhr,
        c.VK_ERROR_OUT_OF_DATE_KHR => VulkanError.OutOfDateKhr,
        c.VK_ERROR_INCOMPATIBLE_DISPLAY_KHR => VulkanError.IncompatibleDisplayKhr,
        c.VK_ERROR_VALIDATION_FAILED_EXT => VulkanError.ValidationFailedExt,
        c.VK_ERROR_INVALID_SHADER_NV => VulkanError.InvalidShaderNv,
        else => VulkanError.UnknownError
    };
}


pub const EngineError = error {
    NoGpuAvailable
};


var render_resources: RenderResources = undefined;


allocator: std.mem.Allocator,
window: *GlfwWindow,
instance: Instance,
surface: c.VkSurfaceKHR,
gpu: PhysicalDevice,
device: Device,

// TODO: option to create multiple samplers
texture_sampler: c.VkSampler,
textures: []Image.Texture,

descriptor_pool: c.VkDescriptorPool,
// TODO: single buffer
uniform_buffers: [utils.MAX_FRAMES_IN_FLIGHT]Buffer,

const main = @import("main.zig");

// TODO: maybe put device as global in `VulkanEngine`
pub fn init(allocator: std.mem.Allocator, app_name: [:0]const u8, debug: bool) !VulkanEngine {
    logger.warn("TODO: maybe pass pointers instead of structs to limit size", .{});
    const window = try GlfwWindow.init(allocator, "Vulkan window");
    const instance = try Instance.init(allocator, app_name, window.get_required_extensions(), debug);
    const surface = try window.create_surface(instance);
    var gpu = instance.find_suitable_gpu(surface) orelse return EngineError.NoGpuAvailable;
    gpu.set_surface(surface);
    logger.info("selected GPU: {s}", .{gpu.properties.deviceName});
    logger.debug("msaa samples: {}", .{gpu.get_sample_counts().max()});

    var device = try Device.init(allocator, gpu);

    create_vma_allocator(instance.get_handle(), gpu.get_handle(), device.get_handle());

    render_resources = try create_render_resources(allocator, device, window);




    main.set_globals(&device, render_resources.command_pool, window.window, &render_resources, undefined);

    // TODO: pass const ?
    var sampler = device.create_texture_sampler();
    const texture = try Image.Texture.init(device, utils.TEXTURE_PATH, true, &sampler);
    const textures = try allocator.alloc(Image.Texture, 1);
    textures[0] = texture;

    main.set_globals(&device, render_resources.command_pool, window.window, &render_resources, undefined);


    try main.load_cube();
    main.create_vertex_buffer();
    main.create_instance_buffer();
    main.create_index_buffer();
    //main.create_uniform_buffers();
    var uniform_buffers = create_uniform_buffers();

    const desc_pool = device.create_descriptor_pool();
    const desc_sets = device.create_descriptor_sets(render_resources.desc_set_layout, desc_pool, texture, uniform_buffers);
    main.set_globals(&device, render_resources.command_pool, window.window, &render_resources, desc_sets);

    main.create_command_buffers();
    main.create_sync_objects();


    main.main_loop(&uniform_buffers);


    return VulkanEngine {
        .allocator = allocator,
        .window = window,
        .instance = instance,
        .surface = surface,
        .gpu = gpu,
        .device = device,
        .textures = textures,
        .texture_sampler = sampler,
        .descriptor_pool = desc_pool,
        .uniform_buffers = uniform_buffers
    };
}

pub fn deinit(self: *VulkanEngine) void {
    for(0..utils.MAX_FRAMES_IN_FLIGHT) |i| {
        self.uniform_buffers[i].deinit();
    }
    const device = self.device.get_handle();

    c.vkDestroyDescriptorPool(device, self.descriptor_pool, VulkanEngine.api_allocator);
    c.vkDestroySampler(device, self.texture_sampler, VulkanEngine.api_allocator);
    for(self.textures) |*texture| texture.deinit();
    self.allocator.free(self.textures);

    cleanup_render_resources(&render_resources);
    c.vkDestroySurfaceKHR(self.instance.get_handle(), self.surface, VulkanEngine.api_allocator);

    destroy_vma_allocator();
    self.device.deinit();
    self.instance.deinit();
    self.window.deinit();
}

pub const UniformBufferObject = struct {
    model: c.mat4,
    view: c.mat4,
    proj: c.mat4
};

pub fn create_uniform_buffers() [utils.MAX_FRAMES_IN_FLIGHT]Buffer {
    const buffer_size = @sizeOf(UniformBufferObject);
    var uniform_buffers: [utils.MAX_FRAMES_IN_FLIGHT]Buffer = undefined;

    for(0..utils.MAX_FRAMES_IN_FLIGHT) |i| {
        uniform_buffers[i] = Buffer.Builder
            .builder(buffer_size)
            .with_usage(c.VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT)
            .with_vma_flags(c.VMA_ALLOCATION_CREATE_MAPPED_BIT | c.VMA_ALLOCATION_CREATE_HOST_ACCESS_RANDOM_BIT)
            .persistent()
            .build();
    }
    return uniform_buffers;
}


pub const RenderResources = struct {
    swap_chain: SwapChain,
    desc_set_layout: c.VkDescriptorSetLayout,
    color_image: Image,
    depth_image: Image,
    render_pass: c.VkRenderPass,
    graphics_pipeline: c.VkPipeline,
    graphics_pipeline_layout: c.VkPipelineLayout,
    framebuffers: []c.VkFramebuffer,
    command_pool: c.VkCommandPool
};

fn create_render_resources(allocator: std.mem.Allocator, device: Device, window: *GlfwWindow) !RenderResources {
    var swap_chain = try SwapChain.init(allocator, device, window);
    const desc_set_layout = try swap_chain.device.create_descriptor_set_layout();
    const images = swap_chain.create_images();
    const render_pass = try swap_chain.create_render_pass();
    const pipeline = try device.create_graphics_pipeline(allocator, render_pass, desc_set_layout);
    const command_pool = try device.create_command_pool(allocator);
    const framebuffers = try swap_chain.create_framebuffers(render_pass, images.color, images.depth);

    return RenderResources {
        .swap_chain = swap_chain,
        .desc_set_layout = desc_set_layout,
        .color_image = images.color,
        .depth_image = images.depth,
        .render_pass = render_pass,
        .graphics_pipeline = pipeline.pipeline,
        .graphics_pipeline_layout = pipeline.layout,
        .framebuffers = framebuffers,
        .command_pool = command_pool
    };
}


pub fn recreate_render_resources(resources: *RenderResources) !void {
    const swap_chain = &resources.swap_chain;
    const window = swap_chain.window.window;
    const device = &swap_chain.device;

    var width: i32 = 0;
    var height: i32 = 0;
    // TODO: set that as a function in GlfwWindow
    c.glfwGetFramebufferSize(window, &width, &height);
    while(width == 0 or height == 0) {
        c.glfwGetFramebufferSize(window, &width, &height);
        c.glfwWaitEvents();
    }
    try emit_error(c.vkDeviceWaitIdle(device.get_handle()));

    // cleanup to do
    resources.color_image.deinit();
    resources.depth_image.deinit();

    for(resources.framebuffers) |frame_buffer| {
        c.vkDestroyFramebuffer(device.get_handle(), frame_buffer, VulkanEngine.api_allocator);
    }
    swap_chain.allocator.free(resources.framebuffers);
    try swap_chain.recreate();
    const images = swap_chain.create_images();
    const framebuffers = try swap_chain.create_framebuffers(resources.render_pass, images.color, images.depth);

    resources.color_image = images.color;
    resources.depth_image = images.depth;
    resources.framebuffers = framebuffers;
}

fn cleanup_render_resources(resources: *RenderResources) void {
    const swap_chain = &resources.swap_chain;
    const device = swap_chain.device.get_handle();

    resources.color_image.deinit();
    resources.depth_image.deinit();
    
    for(resources.framebuffers) |frame_buffer| {
        c.vkDestroyFramebuffer(device, frame_buffer, VulkanEngine.api_allocator);
    }
    swap_chain.allocator.free(resources.framebuffers);
    swap_chain.deinit();


    c.vkDestroyDescriptorSetLayout(device, resources.desc_set_layout, VulkanEngine.api_allocator);

    c.vkDestroyPipeline(device, resources.graphics_pipeline, VulkanEngine.api_allocator);
    c.vkDestroyPipelineLayout(device, resources.graphics_pipeline_layout, VulkanEngine.api_allocator);
    c.vkDestroyRenderPass(device, resources.render_pass, VulkanEngine.api_allocator);

    c.vkDestroyCommandPool(device, resources.command_pool, VulkanEngine.api_allocator);


    resources.* = undefined;
}


// TODO: Texture mapping > images: execute ops in a single command buffer asynchronously
pub fn begin_single_time_commands() c.VkCommandBuffer {
    const alloc_info: c.VkCommandBufferAllocateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .level = c.VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandPool = render_resources.command_pool,
        .commandBufferCount = 1
    };
    var command_buffer: c.VkCommandBuffer = undefined;
    check(c.vkAllocateCommandBuffers(render_resources.swap_chain.device.get_handle(), &alloc_info, &command_buffer));

    const begin_info: c.VkCommandBufferBeginInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .flags = c.VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT
    };

    check(c.vkBeginCommandBuffer(command_buffer, &begin_info));
    return command_buffer;
}

pub fn end_single_time_commands(command_buffer: c.VkCommandBuffer) void {
    check(c.vkEndCommandBuffer(command_buffer));
    const graphics_queue = render_resources.swap_chain.device.graphics_queue.?;

    const submit_info: c.VkSubmitInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .commandBufferCount = 1,
        .pCommandBuffers = &command_buffer
    };
    check(c.vkQueueSubmit(graphics_queue, 1, &submit_info, null));
    check(c.vkQueueWaitIdle(graphics_queue));
    c.vkFreeCommandBuffers(render_resources.swap_chain.device.get_handle(), render_resources.command_pool, 1, &command_buffer);
}





pub fn create_vma_allocator(instance: c.VkInstance, physical_device: c.VkPhysicalDevice, device: c.VkDevice) void {
    const vulkan_functions: c.VmaVulkanFunctions  = .{
        .vkGetInstanceProcAddr = &c.vkGetInstanceProcAddr,
        .vkGetDeviceProcAddr = &c.vkGetDeviceProcAddr
    };

    const memory_callbacks: c.VmaDeviceMemoryCallbacks = .{
        .pfnAllocate = PFN_vmaAllocateDeviceMemoryFunction,
        .pfnFree = PFN_vmaFreeDeviceMemoryFunction
    };

    const allocator_create_info: c.VmaAllocatorCreateInfo = .{
        .flags = c.VMA_ALLOCATOR_CREATE_EXT_MEMORY_BUDGET_BIT,
        .vulkanApiVersion = c.VK_API_VERSION_1_2,
        .physicalDevice = physical_device,
        .device = device,
        .instance = instance,
        .pVulkanFunctions = &vulkan_functions,
        .pDeviceMemoryCallbacks = &memory_callbacks
    };

    check(c.vmaCreateAllocator(&allocator_create_info, &vma_allocator));
}

pub fn destroy_vma_allocator() void {
    c.vmaDestroyAllocator(vma_allocator);
    vma_allocator = null;
}

export fn PFN_vmaAllocateDeviceMemoryFunction(_: c.VmaAllocator, _: u32, memory: c.VkDeviceMemory, size: c.VkDeviceSize, _: ?*anyopaque) void {
    tracy.allocNamed(@ptrCast(memory), size, "VmaAllocator");
}
export fn PFN_vmaFreeDeviceMemoryFunction(_: c.VmaAllocator, _: u32, memory: c.VkDeviceMemory, _: c.VkDeviceSize, _: ?*anyopaque) void {
    tracy.freeNamed(@ptrCast(memory), "VmaAllocator");
}




pub const custom_allocator: ?*const c.VkAllocationCallbacks = null;

// from https://gitlab.freedesktop.org/mesa/mesa/-/blob/main/src/vulkan/util/vk_alloc.c
const MAX_ALIGN = @alignOf(u64);
var addresses = std.hash_map.AutoHashMap(u64, u64).init(std.heap.c_allocator);

export fn PFN_vkAllocationFunction(
    pUserData: ?*anyopaque,
    size: usize,
    alignment: usize,
    allocationScope: c.VkSystemAllocationScope
) ?*anyopaque {
    _ = .{pUserData, allocationScope};

    std.debug.assert(MAX_ALIGN % alignment == 0);
    const ptr = std.c.malloc(size);
    tracy.allocNamed(@ptrCast(ptr), size, "Vulkan API");
    if(addresses.get(@intFromPtr(ptr))) |_| logger.warn("ptr already in", .{});
    addresses.put(@intFromPtr(ptr), 1) catch unreachable;
    return ptr;
}

export fn PFN_vkFreeFunction(pUserData: ?*anyopaque, pMemory: ?*anyopaque) void {
    _ = .{pUserData};
    if(pMemory == null) return;

    // for some reason, there is a single free that was never allocated from our callback,
    // so it messes with tracy. with tracy disabled, it is not a problem
    if(addresses.remove(@intFromPtr(pMemory))) {
        tracy.freeNamed(@ptrCast(pMemory), "Vulkan API");
    }
    std.c.free(pMemory);
}

export fn PFN_vkReallocationFunction(pUserData: ?*anyopaque, pOriginal: ?*anyopaque, size: usize, alignment: usize, allocationScope: c.VkSystemAllocationScope) ?*anyopaque {
    _ = .{pUserData, allocationScope};

    std.debug.assert(MAX_ALIGN % alignment == 0);
    const ptr = std.c.realloc(pOriginal, size);
    if(pOriginal != null) {
        tracy.freeNamed(@ptrCast(pOriginal), "Vulkan API");
    if(!addresses.remove(@intFromPtr(pOriginal))) logger.warn("ptr reallocated not allocated", .{});
    }
    tracy.allocNamed(@ptrCast(ptr), size, "Vulkan API");
    addresses.put(@intFromPtr(ptr), 1) catch unreachable;
    return ptr;
}










pub const InstanceData = struct {
    pos: c.vec3
};

pub const Vertex = struct {
    pos: c.vec3,
    color: c.vec3,
    tex_coord: c.vec2,

    pub fn get_binding_description() c.VkVertexInputBindingDescription {
        const binding_description: c.VkVertexInputBindingDescription = .{
            .binding = 0,
            .stride = @sizeOf(Vertex),
            .inputRate = c.VK_VERTEX_INPUT_RATE_VERTEX
        };

        return binding_description;
    }

    pub fn get_attribute_descriptions() [3]c.VkVertexInputAttributeDescription {
        const attribute_descriptions = [_]c.VkVertexInputAttributeDescription {
            .{
                .binding = 0,
                .location = 0,
                .format = c.VK_FORMAT_R32G32B32_SFLOAT,
                .offset = @offsetOf(Vertex, "pos")
            },
            .{
                .binding = 0,
                .location = 1,
                .format = c.VK_FORMAT_R32G32B32_SFLOAT,
                .offset = @offsetOf(Vertex, "color")
            },
            .{
                .binding = 0,
                .location = 2,
                .format = c.VK_FORMAT_R32G32_SFLOAT,
                .offset = @offsetOf(Vertex, "tex_coord")
            }
        };
        return attribute_descriptions;
    }
};

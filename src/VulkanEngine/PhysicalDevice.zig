const std = @import("std");
const c = @import("../c.zig");
const utils = @import("../utils.zig");
const Allocation = @import("Allocation.zig");
const VulkanEngine = @import("../VulkanEngine.zig");

const logger = utils.Logger(.PhysicalDevice);
const scoped = utils.Scoped(logger);
const check2 = scoped.check2;
const check = scoped.check;
const vk_get_properties = scoped.vk_get_properties;

const PhysicalDevice = @This();

const VkPhysicalDevice = c.VkPhysicalDevice;

pub const AvailableMsaa = struct {
    flags: u8,

    pub fn max(self: AvailableMsaa) u8 {
        return std.math.pow(u8, 2, 8 - @clz(self.flags) - 1);
    }

    pub fn is_count_available(self: AvailableMsaa, count: u8) bool {
        return (self.flags & count) == count;
    }
};


physdev: VkPhysicalDevice,
properties: c.VkPhysicalDeviceProperties,
surface: ?c.VkSurfaceKHR,

pub fn init(physdev: VkPhysicalDevice) PhysicalDevice {
    var properties: c.VkPhysicalDeviceProperties = undefined;
    c.vkGetPhysicalDeviceProperties(physdev, &properties);

    return PhysicalDevice {
        .physdev = physdev,
        .properties = properties,
        .surface = null
    };
}

pub fn set_surface(self: *PhysicalDevice, surface: c.VkSurfaceKHR) void {
    self.surface = surface;
}

pub fn get_surface(self: PhysicalDevice) c.VkSurfaceKHR {
    return self.surface.?;
}

pub fn get_handle(self: PhysicalDevice) VkPhysicalDevice {
    return self.physdev;
}

pub fn is_discrete(self: PhysicalDevice) bool {
    return self.properties.deviceType == c.VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU;
}

pub fn get_sample_counts(self: PhysicalDevice) AvailableMsaa {
    return .{
        .flags = @truncate(self.properties.limits.framebufferColorSampleCounts & self.properties.limits.framebufferDepthSampleCounts)
    };
}


pub fn is_suitable(self: PhysicalDevice, allocator: std.mem.Allocator, surface: c.VkSurfaceKHR) bool {
    const queues_families = self.query_queue_families(allocator);
    defer allocator.free(queues_families);

    for(0..queues_families.len) |i| {
        if(self.queue_supports_present(@intCast(i), surface))
            return true;
    }

    return false;

    // const indices = find_queue_families(allocator, device, surface);
    // const extensions_supported = check_device_extension_support(allocator, device);
    // var swap_chain_adequate = false;
    // if(extensions_supported) {
    //     const swap_chain_support = query_swap_chain_support(allocator, device, surface);
    //     defer allocator.free(swap_chain_support.present_modes);
    //     defer allocator.free(swap_chain_support.formats);

    //     swap_chain_adequate = (swap_chain_support.formats.len > 0) and (swap_chain_support.present_modes.len > 0);
    // }
    // var supported_features: c.VkPhysicalDeviceFeatures = undefined;
    // c.vkGetPhysicalDeviceFeatures(device, &supported_features);
    // return indices.is_complete() and extensions_supported and swap_chain_adequate and supported_features.samplerAnisotropy == c.VK_TRUE;
}


pub const QueueFamilyIndices = struct {
    graphics_family: ?u32,
    present_family: ?u32,

    pub fn is_complete(self: QueueFamilyIndices) bool {
        return self.graphics_family != null and self.present_family != null;
    }
};

pub fn find_queue_families(self: PhysicalDevice, allocator: std.mem.Allocator) QueueFamilyIndices {
    const f = c.vkGetPhysicalDeviceQueueFamilyProperties;
    const queue_families = vk_get_properties(allocator, f, self.get_handle()) catch unreachable;
    defer allocator.free(queue_families);
    var i: u32 = 0;
    var indices: QueueFamilyIndices = .{ 
        .graphics_family = null,
        .present_family = null
    };
    for(queue_families) |family| {
        if((family.queueFlags & c.VK_QUEUE_GRAPHICS_BIT) != 0)
            indices.graphics_family = i;

        var present_support: c.VkBool32 = 0;
        _ = c.vkGetPhysicalDeviceSurfaceSupportKHR(self.get_handle(), i, self.surface.?, &present_support);
        if(present_support != 0)
            indices.present_family = i;

        if(indices.is_complete()) 
            break;
        i += 1;
    }
    return indices;
}

//  const SwapChainSupportDetails = struct {
//      //allocator: std.mem.Allocator,
//      capabilities: c.VkSurfaceCapabilitiesKHR,
//      formats: []c.VkSurfaceFormatKHR,
//      present_modes: []c.VkPresentModeKHR,
//  
//      //pub fn deinit(self: SwapChainSupportDetails) void {
//      //    self.allocator.free(self.formats);
//      //    self.allocator.free(self.present_modes);
//      //}
//  };
//  
//  //const DEVICE_EXTENSIONS = [_][]const u8 { c.VK_KHR_SWAPCHAIN_EXTENSION_NAME };
//  
//  pub fn query_swap_chain_support(allocator: std.mem.Allocator, physical_device: c.VkPhysicalDevice, surface: c.VkSurfaceKHR) SwapChainSupportDetails {
//      const f1 = c.vkGetPhysicalDeviceSurfaceFormatsKHR;
//      const f2 = c.vkGetPhysicalDeviceSurfacePresentModesKHR;
//      var details: SwapChainSupportDetails = .{
//          //.allocator = allocator,
//          .capabilities = undefined,
//          .formats = vk_get_properties(allocator, f1, .{physical_device, surface}) catch unreachable,
//          .present_modes = vk_get_properties(allocator, f2, .{physical_device, surface}) catch unreachable,
//      };
//      check(c.vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device, surface, &details.capabilities));
//  
//      return details;
//  }
//  
//  
//  fn check_device_extension_support(self: PhysicalDevice, allocator: std.mem.Allocator) bool {
//      const available_extensions: []c.VkExtensionProperties = vk_get_properties(allocator, c.vkEnumerateDeviceExtensionProperties, .{self.get_handle(), null}) catch unreachable;
//      defer allocator.free(available_extensions);
//  
//      for(DEVICE_EXTENSIONS) |required_extension| {
//          var found: bool = false;
//          for(available_extensions) |ext| {
//              const name = std.mem.span(@as([*c]const u8, &ext.extensionName));
//              if(std.mem.eql(u8, name, required_extension))
//                  found = true;
//          }
//          if(!found) return false;
//      }
//  
//      return true;
//  }

pub fn query_queue_families(self: PhysicalDevice, allocator: std.mem.Allocator) []c.VkQueueFamilyProperties {
    const f = c.vkGetPhysicalDeviceQueueFamilyProperties;
    const queue_families = vk_get_properties(allocator, f, self.get_handle()) catch unreachable;
    return queue_families;
    // var i: u32 = 0;
    // var indices: QueueFamilyIndices = .{ 
    //     .graphics_family = null,
    //     .present_family = null
    // };
    // for(queue_families) |family| {
    //     if((family.queueFlags & c.VK_QUEUE_GRAPHICS_BIT) != 0)
    //         indices.graphics_family = i;

    //     var present_support: c.VkBool32 = 0;
    //     _ = c.vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &present_support);
    //     if(present_support != 0)
    //         indices.present_family = i;

    //     if(indices.is_complete()) 
    //         break;
    //     i += 1;
    // }
    // return indices;
}

fn queue_supports_present(self: PhysicalDevice, queue_id: u32, surface: c.VkSurfaceKHR) bool {
    var present_support: c.VkBool32 = 0;
    check(c.vkGetPhysicalDeviceSurfaceSupportKHR(self.get_handle(), queue_id, surface, &present_support));
    return present_support == c.VK_TRUE;
}

fn find_supported_format(self: PhysicalDevice, candidates: []c.VkFormat, tiling: c.VkImageTiling, features: c.VkFormatFeatureFlags) c.VkFormat {
    for(candidates) |format| {
        var props: c.VkFormatProperties = undefined;
        c.vkGetPhysicalDeviceFormatProperties(self.get_handle(), format, &props);
        if(tiling == c.VK_IMAGE_TILING_LINEAR and (props.linearTilingFeatures & features) == features) {
            return format;
        } else if(tiling == c.VK_IMAGE_TILING_OPTIMAL and (props.optimalTilingFeatures & features) == features) {
            return format;
        }
    }
    logger.err("failed to find supported format", .{});
    std.process.exit(1);
}

pub fn find_depth_format(self: PhysicalDevice) c.VkFormat {
    const candidates = [_]c.VkFormat { c.VK_FORMAT_D32_SFLOAT, c.VK_FORMAT_D32_SFLOAT_S8_UINT, c.VK_FORMAT_D24_UNORM_S8_UINT };
    return self.find_supported_format(
        @constCast(&candidates),
        c.VK_IMAGE_TILING_OPTIMAL,
        c.VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
    );
}

const std = @import("std");
const c = @import("../c.zig");
const utils = @import("../utils.zig");
const Allocation = @import("Allocation.zig");
const VulkanEngine = @import("../VulkanEngine.zig");

const logger = utils.Logger(.SwapChain);
const scoped = utils.Scoped(logger);
const check2 = scoped.check2;
const check = scoped.check;
const vk_get_properties = scoped.vk_get_properties;

const SwapChain = @This();
const Device = VulkanEngine.Device;
const Image = VulkanEngine.Image;
const ImageBuilder = Image.Builder;
const GlfwWindow = VulkanEngine.GlfwWindow;

const VkSwapChain = c.VkSwapchainKHR;

allocator: std.mem.Allocator,
device: Device,
window: *GlfwWindow,

swap_chain: ?VkSwapChain,
images: ?[]Image,
extent: ?c.VkExtent3D,
format: ?c.VkFormat,

color_image: ?Image,
depth_image: ?Image,

pub fn init(allocator: std.mem.Allocator, device: Device, window: *GlfwWindow) !SwapChain {
    var swap_chain = SwapChain {
        .allocator = allocator,
        .device = device,
        .window = window,
        .swap_chain = null,
        .images = null,
        .extent = null,
        .format = null,
        .color_image = null,
        .depth_image = null
    };

    try swap_chain.create();



    return swap_chain;
}

pub fn get_handle(self: SwapChain) c.VkSwapchainKHR {
    return self.swap_chain.?;
}

pub fn create(self: *SwapChain) !void {
    const device = self.device;
    const gpu = device.gpu;

    const swap_chain_support = query_swap_chain_support(self.allocator, gpu.get_handle(), gpu.get_surface());
    const surface_format = choose_swap_surface_format(swap_chain_support.formats);
    const present_mode = choose_swap_present_mode(swap_chain_support.present_modes);
    const extent = choose_swap_extent(swap_chain_support.capabilities, self.window.window);
    var image_count = swap_chain_support.capabilities.minImageCount + 1;
    if(swap_chain_support.capabilities.maxImageCount > 0 and image_count > swap_chain_support.capabilities.maxImageCount)
        image_count = swap_chain_support.capabilities.maxImageCount;

    const _indices = device.gpu.find_queue_families(self.allocator);
    const queue_family_indices = [_]u32 {_indices.graphics_family.?, _indices.present_family.?};
    const families_differ = _indices.graphics_family.? != _indices.present_family.?;

    const create_info: c.VkSwapchainCreateInfoKHR = .{
        .sType = c.VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
        .surface = device.gpu.get_surface(),
        .minImageCount = image_count,
        .imageFormat = surface_format.format,
        .imageColorSpace = surface_format.colorSpace,
        .imageExtent = extent,
        .imageArrayLayers = 1,
        .imageUsage = c.VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
        .preTransform = swap_chain_support.capabilities.currentTransform,
        .compositeAlpha = c.VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        .presentMode = present_mode,
        .clipped = c.VK_TRUE,
        .oldSwapchain = null,
        .imageSharingMode = if(families_differ) c.VK_SHARING_MODE_CONCURRENT else c.VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = if(families_differ) 2 else 0,
        .pQueueFamilyIndices = if(families_differ) @ptrCast(&queue_family_indices) else null
    };

    var swap_chain: VkSwapChain = undefined;
    try VulkanEngine.emit_error(c.vkCreateSwapchainKHR(device.get_handle(), &create_info, VulkanEngine.api_allocator, &swap_chain));

    try VulkanEngine.emit_error(c.vkGetSwapchainImagesKHR(device.get_handle(), swap_chain, &image_count, null));
    const swap_chain_images = self.allocator.alloc(Image, image_count) catch unreachable;
    const images = self.allocator.alloc(c.VkImage, image_count) catch unreachable;
    defer self.allocator.free(images);
    try VulkanEngine.emit_error(c.vkGetSwapchainImagesKHR(device.get_handle(), swap_chain, &image_count, images.ptr));

    for(0..image_count) |i| {
        swap_chain_images[i] = Image.from(images[i], .{ .width = extent.width, .height = extent.height, .depth = 1 }, surface_format.format, 1, c.VK_IMAGE_LAYOUT_UNDEFINED);
        swap_chain_images[i].create_image_view(device, c.VK_IMAGE_ASPECT_COLOR_BIT);
    }


    self.swap_chain = swap_chain;
    self.images = swap_chain_images;
    self.extent = swap_chain_images[0].extent;
    self.format = swap_chain_images[0].format;
}


pub fn create_framebuffers(self: SwapChain, render_pass: c.VkRenderPass, color_image: Image, depth_image: Image) ![]c.VkFramebuffer {
    if(self.images == null) {
        logger.err("creating framebuffers when not created", .{});
        @panic("");
    }
    const images = self.images.?;
    const swap_chain_framebuffers = try self.allocator.alloc(c.VkFramebuffer, images.len);

    for(0..images.len) |i| {
        const view = images[i].get_view_handle();
        const attachments = [_]c.VkImageView { color_image.get_view_handle(), depth_image.get_view_handle(), view };
        const framebuffer_info: c.VkFramebufferCreateInfo = .{
            .sType = c.VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
            .renderPass = render_pass,
            .attachmentCount = attachments.len,
            .pAttachments = &attachments,
            .width = self.extent.?.width,
            .height = self.extent.?.height,
            .layers = 1
        };

        try VulkanEngine.emit_error(c.vkCreateFramebuffer(self.device.get_handle(), &framebuffer_info, VulkanEngine.api_allocator, &swap_chain_framebuffers[i]));
    }
    return swap_chain_framebuffers;
}

const Images = struct {
    color: Image,
    depth: Image
};
pub fn create_images(self: SwapChain) Images {
    const msaa_samples = self.device.gpu.get_sample_counts().max();

    var color_image = ImageBuilder
        .builder(self.extent.?)
        .with_sample_count(msaa_samples)
        .with_format(self.format.?)
        .with_usage(c.VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | c.VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT)
        .with_vma_usage(c.VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
        .build();
    color_image.create_image_view(self.device, c.VK_IMAGE_ASPECT_COLOR_BIT);
    
    const depth_format = self.device.gpu.find_depth_format();
    var depth_image = ImageBuilder
        .builder(self.extent.?)
        .with_sample_count(msaa_samples)
        .with_format(depth_format)
        .with_usage(c.VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT)
        .with_vma_usage(c.VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
        .build();
    depth_image.create_image_view(self.device, c.VK_IMAGE_ASPECT_DEPTH_BIT);

    return Images {
        .color = color_image,
        .depth = depth_image
    };
}

pub fn cleanup(self: *SwapChain) void {
    if(self.images) |images| {
        for(images) |*image| {
            image.deinit();
        }
        self.allocator.free(images);
        self.images = null;
    }
    if(self.swap_chain) |swap_chain|
        c.vkDestroySwapchainKHR(self.device.get_handle(), swap_chain, VulkanEngine.api_allocator);
}

pub fn recreate(self: *SwapChain) !void {
    self.cleanup();
    try self.create();
}

pub fn deinit(self: *SwapChain) void {
    self.cleanup();
    
    self.* = undefined;
}

pub fn get_aspect_ration(self: SwapChain) f32 {
    return @as(f32, @floatFromInt(self.extent.width)) / @as(f32, @floatFromInt(self.extent.height));
}

pub fn create_render_pass(self: SwapChain) !c.VkRenderPass {
    const msaa_samples = self.device.gpu.get_sample_counts().max();
    const color_attachment: c.VkAttachmentDescription = .{
        .format = self.format.?,
        .samples = msaa_samples,
        .loadOp = c.VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = c.VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = c.VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = c.VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = c.VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = c.VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    };

    const depth_attachment: c.VkAttachmentDescription = .{
        .format = self.device.gpu.find_depth_format(),
        .samples = msaa_samples,
        .loadOp = c.VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = c.VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .stencilLoadOp = c.VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = c.VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = c.VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = c.VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
    };
    
    const color_attachment_resolve: c.VkAttachmentDescription = .{
        .format = self.format.?,
        .samples = c.VK_SAMPLE_COUNT_1_BIT,
        .loadOp = c.VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .storeOp = c.VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = c.VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = c.VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = c.VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = c.VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
    };

    const color_attachment_ref: c.VkAttachmentReference = .{
        .attachment = 0,
        .layout = c.VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    };

    const depth_attachment_ref: c.VkAttachmentReference = .{
        .attachment = 1,
        .layout = c.VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
    };

    const color_attachment_resolve_ref: c.VkAttachmentReference = .{
        .attachment = 2,
        .layout = c.VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    };

    const subpass: c.VkSubpassDescription = .{
        .pipelineBindPoint = c.VK_PIPELINE_BIND_POINT_GRAPHICS,
        .colorAttachmentCount = 1,
        .pColorAttachments = &color_attachment_ref,
        .pDepthStencilAttachment = &depth_attachment_ref,
        .pResolveAttachments = &color_attachment_resolve_ref
    };

    const dependency: c.VkSubpassDependency = .{
        .srcSubpass = c.VK_SUBPASS_EXTERNAL,
        .dstSubpass = 0,
        .srcStageMask = c.VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | c.VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT,
        .srcAccessMask = c.VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | c.VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
        .dstStageMask = c.VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | c.VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
        .dstAccessMask = c.VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | c.VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT
    };

    const attachments = [_]c.VkAttachmentDescription { color_attachment, depth_attachment, color_attachment_resolve };

    const render_pass_info: c.VkRenderPassCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .attachmentCount = attachments.len,
        .pAttachments = &attachments,
        .subpassCount = 1,
        .pSubpasses = &subpass,
        .dependencyCount = 1,
        .pDependencies = &dependency
    };

    var render_pass: c.VkRenderPass = undefined;
    try VulkanEngine.emit_error(c.vkCreateRenderPass(self.device.get_handle(), &render_pass_info, VulkanEngine.api_allocator, &render_pass));
    return render_pass;
}



const SwapChainSupportDetails = struct {
    //allocator: std.mem.Allocator,
    capabilities: c.VkSurfaceCapabilitiesKHR,
    formats: []c.VkSurfaceFormatKHR,
    present_modes: []c.VkPresentModeKHR,

    //pub fn deinit(self: SwapChainSupportDetails) void {
    //    self.allocator.free(self.formats);
    //    self.allocator.free(self.present_modes);
    //}
};

pub fn query_swap_chain_support(allocator: std.mem.Allocator, physical_device: c.VkPhysicalDevice, surface: c.VkSurfaceKHR) SwapChainSupportDetails {
    const f1 = c.vkGetPhysicalDeviceSurfaceFormatsKHR;
    const f2 = c.vkGetPhysicalDeviceSurfacePresentModesKHR;
    var details: SwapChainSupportDetails = .{
        //.allocator = allocator,
        .capabilities = undefined,
        .formats = vk_get_properties(allocator, f1, .{physical_device, surface}) catch unreachable,
        .present_modes = vk_get_properties(allocator, f2, .{physical_device, surface}) catch unreachable,
    };
    check(c.vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device, surface, &details.capabilities));

    return details;
}



fn choose_swap_surface_format(available_formats: []c.VkSurfaceFormatKHR) c.VkSurfaceFormatKHR {
    for(available_formats) |available_format| {
        if(available_format.format == c.VK_FORMAT_B8G8R8A8_SRGB and available_format.colorSpace == c.VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
            return available_format;
        }
    }
    return available_formats[0];
}

fn choose_swap_present_mode(available_present_modes: []c.VkPresentModeKHR) c.VkPresentModeKHR {
    for(available_present_modes) |available_present_mode| {
        if(available_present_mode == c.VK_PRESENT_MODE_MAILBOX_KHR) {
            return available_present_mode;
        }
    }
    return c.VK_PRESENT_MODE_FIFO_KHR;
}

fn choose_swap_extent(capabilities: c.VkSurfaceCapabilitiesKHR, window: ?*c.GLFWwindow) c.VkExtent2D {
    if(capabilities.currentExtent.width != std.math.maxInt(u32)) {
        return capabilities.currentExtent;
    } else {
        var width: i32 = undefined;
        var height: i32 = undefined;
        c.glfwGetFramebufferSize(window, &width, &height);

        const actual_extent: c.VkExtent2D = .{
            .width = std.math.clamp(@as(u32, @bitCast(width)), capabilities.minImageExtent.width, capabilities.maxImageExtent.width),
            .height = std.math.clamp(@as(u32, @bitCast(height)), capabilities.minImageExtent.height, capabilities.maxImageExtent.height),
        };

        return actual_extent;
    }
}

fn has_stencil_component(format: c.VkFormat) bool {
    return format == c.VK_FORMAT_D32_SFLOAT_S8_UINT or format == c.VK_FORMAT_D24_UNORM_S8_UINT;
}


const std = @import("std");
const c = @import("../c.zig");
const utils = @import("../utils.zig");
const Allocation = @import("Allocation.zig");
const VulkanEngine = @import("../VulkanEngine.zig");

const logger = utils.Logger(.Buffer);
const scoped = utils.Scoped(logger);
const check2 = scoped.check2;
const check = scoped.check;

const Buffer = @This();
const VkBuffer = c.VkBuffer;
const VmaAllocator = c.VmaAllocator;
const VmaAllocation = c.VmaAllocation;

buffer: VkBuffer,
allocation: Allocation,

fn init(alloc_info: c.VmaAllocationCreateInfo, create_info: c.VkBufferCreateInfo, persistent: bool) Buffer {
    var buffer: c.VkBuffer = undefined;
    var allocation: VmaAllocation = undefined;
    check2(c.vmaCreateBuffer(VulkanEngine.vma_allocator, &create_info, &alloc_info, &buffer, &allocation, null), "failed to create buffer");
    return .{
        .buffer = buffer,
        .allocation = Allocation.init(allocation, persistent)
    };
}

pub fn deinit(self: *Buffer) void {
    self.allocation.unmap();
    c.vmaDestroyBuffer(VulkanEngine.vma_allocator, self.buffer, self.allocation.get_handle());

    self.* = undefined;
}

pub fn get_handle(self: Buffer) VkBuffer {
    return self.buffer;
}


pub fn create_staging_buffer(size: c.VkDeviceSize, data: anytype) Buffer {
    var buffer = Builder
        .builder(size)
        .with_vma_flags(c.VMA_ALLOCATION_CREATE_MAPPED_BIT | c.VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT)
        .with_usage(c.VK_IMAGE_USAGE_TRANSFER_SRC_BIT)
        .build();
    buffer.allocation.write_data(data, size, 0);
    return buffer;
}

pub fn copy_to(self: Buffer, dst: Buffer, size: c.VkDeviceSize) void {
    const command_buffer = VulkanEngine.begin_single_time_commands();
    defer VulkanEngine.end_single_time_commands(command_buffer);

    const copy_region: c.VkBufferCopy = .{
        .size = size
    };
    c.vkCmdCopyBuffer(command_buffer, self.get_handle(), dst.get_handle(), 1, &copy_region);
}

// TODO: maybe use image.layout in `vkCmdCopyBufferToImage`
pub fn copy_to_image(self: Buffer, image: VulkanEngine.Image) void {
    std.debug.assert(image.layout == c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    const command_buffer = VulkanEngine.begin_single_time_commands();
    defer VulkanEngine.end_single_time_commands(command_buffer);

    const region: c.VkBufferImageCopy = .{
        .bufferOffset = 0,
        .bufferRowLength = 0,
        .bufferImageHeight = 0,
        .imageSubresource = .{
            .aspectMask = c.VK_IMAGE_ASPECT_COLOR_BIT,
            .mipLevel = 0,
            .baseArrayLayer = 0,
            .layerCount = 1
        },
        .imageOffset = .{ .x = 0, .y = 0, .z = 0 },
        .imageExtent = .{
            .width = image.width(),
            .height = image.height(),
            .depth = 1
        }
    };
    c.vkCmdCopyBufferToImage(command_buffer, self.get_handle(), image.get_handle(), c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);
}



pub const Builder = struct {
    alloc_info: c.VmaAllocationCreateInfo,
    create_info: c.VkBufferCreateInfo,
    persists: bool,

    pub fn builder(size: c.VkDeviceSize) Builder {
        return .{
            .alloc_info = .{
                .usage = c.VMA_MEMORY_USAGE_AUTO
            },
            .create_info = .{
                .sType = c.VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
                .size = size
            },
            .persists = false
        };
    }

    pub fn build(self: Builder) Buffer {
        return init(self.alloc_info, self.create_info, self.persists);
    }

    pub fn with_usage(self: Builder, usage: c.VkBufferUsageFlags) Builder {
        var _builder = self;
        _builder.create_info.usage = usage;
        return _builder;
    }
    
    pub fn with_sharing_mode(self: Builder, sharing: c.VkSharingMode) Builder {
        var _builder = self;
        _builder.create_info.sharingMode = sharing;
        return _builder;
    }

    pub fn with_flags(self: Builder, flags: c.VkBufferCreateFlags) Builder {
        var _builder = self;
        _builder.create_info.flags = flags;
        return _builder;
    }

    pub fn persistent(self: Builder) Builder {
        var _builder = self;
        _builder.persists = true;
        return _builder;
    }
    

    pub fn with_vma_usage(self: Builder, usage: c.VmaMemoryUsage) Builder {
        var _builder = self;
        _builder.alloc_info.usage = usage;
        return _builder;
    }

    pub fn with_vma_flags(self: Builder, flags: c.VmaAllocationCreateFlags) Builder {
        var _builder = self;
        _builder.alloc_info.flags = flags;
        return _builder;
    }
};

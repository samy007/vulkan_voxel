const std = @import("std");
const c = @import("../c.zig");
const utils = @import("../utils.zig");
const VulkanEngine = @import("../VulkanEngine.zig");

const logger = utils.Logger(.GlfWindow);
const scoped = utils.Scoped(logger);
const check2 = scoped.check2;
const check = scoped.check;

const GlfwWindow = @This();

const GLFWwindow = ?*c.GLFWwindow;

const GlfwError = error {
    InitFailed,
    UnknownError,
    SurfaceCreationFailed
};

window: GLFWwindow,
allocator: std.mem.Allocator,
focused: bool,


pub fn init(allocator: std.mem.Allocator, window_name: [*c]const u8) GlfwError!*GlfwWindow {
    if(c.glfwInit() == 0) {
        return GlfwError.InitFailed;
    }
    _ = c.glfwSetErrorCallback(error_callback);
    c.glfwWindowHint(c.GLFW_CLIENT_API, c.GLFW_NO_API);
    //const monitor = c.glfwGetPrimaryMonitor();
    //const mode = c.glfwGetVideoMode(monitor);
    //const window = c.glfwCreateWindow(mode.*.width, mode.*.height, window_name, monitor, null);
    const window = c.glfwCreateWindow(800, 450, window_name, null, null);
    if(window == null) 
        return GlfwError.UnknownError;
    
    const glfw_window = allocator.create(GlfwWindow) catch return GlfwError.UnknownError;
    glfw_window.* = .{
        .window = window,
        .allocator = allocator,
        .focused = false, 
    };
    
    c.glfwSetWindowUserPointer(window, @ptrCast(glfw_window));

    _ = c.glfwSetWindowCloseCallback(window, window_close_callback);
    _ = c.glfwSetWindowSizeCallback(window, window_size_callback);
    _ = c.glfwSetWindowFocusCallback(window, window_focus_callback);
    _ = c.glfwSetKeyCallback(window, key_callback);
    _ = c.glfwSetCursorPosCallback(window, cursor_position_callback);
    _ = c.glfwSetMouseButtonCallback(window, mouse_button_callback);

    c.glfwSetInputMode(window, c.GLFW_STICKY_KEYS, 1);
    c.glfwSetInputMode(window, c.GLFW_STICKY_MOUSE_BUTTONS, 1);


    return glfw_window;
}

pub fn deinit(self: *GlfwWindow) void {
    c.glfwDestroyWindow(self.window);
    c.glfwTerminate();
    self.allocator.destroy(self);
}


pub fn create_surface(self: GlfwWindow, instance: VulkanEngine.Instance) GlfwError!c.VkSurfaceKHR {
    var surface: c.VkSurfaceKHR = undefined;
    const res = c.glfwCreateWindowSurface(instance.get_handle(), self.window, VulkanEngine.api_allocator, &surface);
    if(res != c.VK_SUCCESS)
        return GlfwError.SurfaceCreationFailed;
    return surface;
}

pub fn get_required_extensions(_: GlfwWindow) [][*c] const u8 {
    var extension_count: u32 = 0;
    const exts = c.glfwGetRequiredInstanceExtensions(&extension_count);
    return exts[0..extension_count];
}



export fn error_callback(err: i32, desc: [*c]const u8) void {
    logger.err("GLFW error (code {}): {s}", .{err, std.mem.span(desc)});
}

export fn window_close_callback(window: GLFWwindow) void {
    c.glfwSetWindowShouldClose(window, c.GLFW_TRUE);
}

export fn window_size_callback(_: GLFWwindow, _: i32, _: i32) void {
    logger.err("TODO: handle glfw resize", .{});
}

export fn window_focus_callback(window: GLFWwindow, focused: i32) void {
    const glfw_window = get_window_pointer(window);
    glfw_window.focused = focused != 0;
}

export fn key_callback(window: ?*c.GLFWwindow, key: i32, _: i32, action: i32, _: i32) void {
    const glfw_window = get_window_pointer(window);
    _ = .{glfw_window, key, action};
    //const pressed = action == c.GLFW_PRESS or action == c.GLFW_REPEAT;
    //switch(key) {
    //    c.GLFW_KEY_UP => input_info.key.up = pressed,
    //    c.GLFW_KEY_DOWN => input_info.key.down = pressed,
    //    c.GLFW_KEY_LEFT => input_info.key.left = pressed,
    //    c.GLFW_KEY_RIGHT => input_info.key.right = pressed,
    //    else => {}
    //}
}

export fn cursor_position_callback(_: ?*c.GLFWwindow, x: f64, y: f64) void {
    _ = .{x, y};
    // input_info.mouse.dx = x - input_info.mouse.x;
    // input_info.mouse.dy = y - input_info.mouse.y;
    // input_info.mouse.x = x;
    // input_info.mouse.y = y;
       
    // const speed = 0.07;
    // const DEG_TO_RAD = speed * 2 * std.math.pi / 180.0;
    // camera.rotate(@floatCast(-input_info.mouse.dx * DEG_TO_RAD), @floatCast(-input_info.mouse.dy * DEG_TO_RAD), 0.0);
}
export fn mouse_button_callback(_: ?*c.GLFWwindow, button: i32, action: i32, _: i32) void {
    _ = .{button, action};
    // const pressed = action == c.GLFW_PRESS;
    // switch(button) {
    //     c.GLFW_MOUSE_BUTTON_LEFT => input_info.mouse.left_pressed = pressed,
    //     c.GLFW_MOUSE_BUTTON_RIGHT => input_info.mouse.right_pressed = pressed,
    //     c.GLFW_MOUSE_BUTTON_MIDDLE => input_info.mouse.middle_pressed = pressed,
    //     else => {}
    // }
}

fn get_window_pointer(window: GLFWwindow) *GlfwWindow {
    return @alignCast(@ptrCast(c.glfwGetWindowUserPointer(window)));
}

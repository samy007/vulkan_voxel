const std = @import("std");
const c = @import("../c.zig");
const utils = @import("../utils.zig");
const VulkanEngine = @import("../VulkanEngine.zig");

const logger = utils.Logger(.Allocation);
const scoped = utils.Scoped(logger);
const check2 = scoped.check2;
const check = scoped.check;

const Allocation = @This();

vma_allocation: c.VmaAllocation,
persistent: bool,
coherent: bool,
mapped_data: ?[*]u8,


pub fn init(allocation: c.VmaAllocation, persistent: bool) Allocation {
    var mem_properties: c.VkMemoryPropertyFlags = undefined;
    c.vmaGetAllocationMemoryProperties(VulkanEngine.vma_allocator, allocation, &mem_properties);

    var self: Allocation = .{
        .vma_allocation = allocation,
        .coherent = (mem_properties & c.VK_MEMORY_PROPERTY_HOST_COHERENT_BIT) == c.VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        .mapped_data = null,
        .persistent = persistent
    };
    if(persistent)
        self.map();
    return self;
}

pub fn get_handle(self: Allocation) c.VmaAllocation {
    return self.vma_allocation;
}

pub fn mapped(self: *Allocation) bool {
    return self.mapped_data != null;
}

pub fn flush(self: *Allocation, offset: c.VkDeviceSize, size: c.VkDeviceSize) void {
    if(!self.coherent)
        check(c.vmaFlushAllocation(VulkanEngine.vma_allocator, self.vma_allocation, offset, size));
}

pub fn flush_all(self: *Allocation) void {
    self.flush(0, c.VK_WHOLE_SIZE);
}

// TODO: make functions like that faillible
pub fn map(self: *Allocation) void {
    if(!self.mapped()) {
        var data: ?*anyopaque = undefined;
        check(c.vmaMapMemory(VulkanEngine.vma_allocator, self.vma_allocation, &data));
        std.debug.assert(data != null);

        self.mapped_data = @ptrCast(data);
    }
    // return self.mapped_data;
}

pub fn unmap(self: *Allocation) void {
    if(self.mapped()) {
        c.vmaUnmapMemory(VulkanEngine.vma_allocator, self.vma_allocation);
        self.mapped_data = null;
    }
}

pub fn write_data(self: *Allocation, data: anytype, size: usize, offset: usize) void {
    if(self.persistent) {
        _ = std.zig.c_builtins.__builtin_memcpy(self.mapped_data.? + offset, @ptrCast(data), size);
        self.flush_all();
    } else {
        self.map();
        _ = std.zig.c_builtins.__builtin_memcpy(self.mapped_data.? + offset, @ptrCast(data), size);
        self.flush_all();
        self.unmap();
    }
}

const std = @import("std");
const c = @import("../c.zig");
const utils = @import("../utils.zig");
const VulkanEngine = @import("../VulkanEngine.zig");

const logger = utils.Logger(.Instance);
const scoped = utils.Scoped(logger);
const check2 = scoped.check2;
const check = scoped.check;
const vk_get_properties = scoped.vk_get_properties;

const Instance = @This();

const PhysicalDevice = VulkanEngine.PhysicalDevice;
const Allocator = std.mem.Allocator;
const VkInstance = c.VkInstance;

// TODO: remove pub
pub const VALIDATION_LAYERS = [_][]const u8 { "VK_LAYER_KHRONOS_validation" };

instance: VkInstance,
allocator: Allocator,

gpus: ?[]PhysicalDevice,

debug_messenger: ?c.VkDebugUtilsMessengerEXT,

pub const InstanceCreationError = error {
    ValidationLayerNotFound,
};

pub fn init(allocator: Allocator, app_name: [:0]const u8, required_extensions: [][*c]const u8, enable_validation_layers: bool) !Instance {
    logger.warn("remove pub from validation_layers const", .{});
    if(enable_validation_layers and !check_validation_layer_support(allocator))
        return InstanceCreationError.ValidationLayerNotFound;

    const app_info: c.VkApplicationInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_APPLICATION_INFO,
        .pApplicationName = app_name,
        .applicationVersion = c.VK_MAKE_VERSION(0, 1, 0),
        .pEngineName = "VulkanEngine",
        .engineVersion = c.VK_MAKE_VERSION(0, 1, 0),
        .apiVersion = c.VK_API_VERSION_1_3
    };

    var exts = required_extensions;
    if(enable_validation_layers) {
        exts = try allocator.alloc([*c]const u8, required_extensions.len + 1);
        @memcpy(exts[0..required_extensions.len], required_extensions);
        exts[required_extensions.len] = c.VK_EXT_DEBUG_UTILS_EXTENSION_NAME;
    }
    defer if(enable_validation_layers) allocator.free(exts);

    const debug_create_info: c.VkDebugUtilsMessengerCreateInfoEXT = .{
        .sType = c.VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
        .messageSeverity = c.VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | c.VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | c.VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
        .messageType = c.VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | c.VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | c.VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
        .pfnUserCallback = debug_callback
    };
    const create_info: c.VkInstanceCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        .pApplicationInfo = &app_info,
        .enabledExtensionCount = @intCast(exts.len),
        .ppEnabledExtensionNames = @ptrCast(exts),
        .enabledLayerCount = if(enable_validation_layers) VALIDATION_LAYERS.len else 0,
        .ppEnabledLayerNames = if(enable_validation_layers) @ptrCast(&VALIDATION_LAYERS) else null,
        .pNext = if(enable_validation_layers) &debug_create_info else null
    };

    var inst: VkInstance = undefined;

    try VulkanEngine.emit_error(c.vkCreateInstance(&create_info, VulkanEngine.api_allocator, &inst));

    var instance = Instance { 
        .instance = inst,
        .allocator = allocator, 
        .debug_messenger = null, 
        .gpus = null 
    };

    if(enable_validation_layers) {
        var debug_messenger: c.VkDebugUtilsMessengerEXT = undefined;
        const vkCreateDebugUtilsMessengerEXT: c.PFN_vkCreateDebugUtilsMessengerEXT = @ptrCast(c.vkGetInstanceProcAddr(instance.get_handle(), "vkCreateDebugUtilsMessengerEXT"));
        check2(vkCreateDebugUtilsMessengerEXT.?(instance.get_handle(), &debug_create_info, VulkanEngine.api_allocator, &debug_messenger), "failed to create debug messenger");
        instance.debug_messenger = debug_messenger;
    }

    // TODO: maybe deinit instance if this fails
    //try instance.setup();
    try instance.query_gpus();

    return instance;
}


pub fn deinit(self: *Instance) void {
    if(self.gpus) |gpus| {
        self.allocator.free(gpus);
    }
    if(self.debug_messenger) |debug_messenger| {
        const vkDestroyDebugUtilsMessengerEXT: c.PFN_vkDestroyDebugUtilsMessengerEXT = @ptrCast(c.vkGetInstanceProcAddr(self.get_handle(), "vkDestroyDebugUtilsMessengerEXT"));
        vkDestroyDebugUtilsMessengerEXT.?(self.get_handle(), debug_messenger, VulkanEngine.api_allocator);
        self.debug_messenger = null;
    }
    c.vkDestroyInstance(self.get_handle(), VulkanEngine.api_allocator);
    self.instance = null;
}

pub fn query_gpus(self: *Instance) !void {
    const devices = try vk_get_properties(self.allocator, c.vkEnumeratePhysicalDevices, self.get_handle());
    defer self.allocator.free(devices);

    const gpus = try self.allocator.alloc(PhysicalDevice, devices.len);
    self.gpus = gpus;
    for(devices, 0..) |device, i| {
        gpus[i] = PhysicalDevice.init(device);
    }
}

pub fn find_suitable_gpu(self: Instance, surface: c.VkSurfaceKHR) ?PhysicalDevice {
    if(self.gpus) |gpus| {
        for(gpus) |gpu| {
            if(gpu.is_suitable(self.allocator, surface) and gpu.is_discrete())
                return gpu;
        }

        logger.warn("did not found any discrete gpu, falling back to any gpu", .{});
        for(gpus) |gpu| {
            if(gpu.is_suitable(self.allocator, surface))
                return gpu;
        }
    }
    return null;
}

pub fn get_handle(self: Instance) VkInstance {
    return self.instance.?;
}


fn check_validation_layer_support(allocator: Allocator) bool {
    const f = c.vkEnumerateInstanceLayerProperties;
    const available_layers = vk_get_properties(allocator, f, undefined) catch unreachable;
    defer allocator.free(available_layers);

    for(VALIDATION_LAYERS) |layer| {
        var layer_found: bool = false;
        for(available_layers) |available_layer| {
            const av_layer_name = std.mem.span(@as([*c]const u8, (&available_layer.layerName)));
            logger.debug("available layer: {s}", .{av_layer_name});
            if(std.mem.eql(u8, layer, av_layer_name)) {
                layer_found = true;
                break;
            }
        }
        if(!layer_found) {
            return false;
        }
    }
    return true;
}


const validation_layer_logger = utils.Logger(.validation_layer);

export fn debug_callback(
    _: c.VkDebugUtilsMessageSeverityFlagBitsEXT,
    _: c.VkDebugUtilsMessageTypeFlagsEXT,
    pCallbackData: ?*const c.VkDebugUtilsMessengerCallbackDataEXT,
    _: ?*anyopaque
) c.VkBool32 {
    validation_layer_logger.debug("{s}", .{std.mem.span(pCallbackData.?.pMessage)});
    return c.VK_FALSE;
}

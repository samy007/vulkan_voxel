const std = @import("std");
const c = @import("../c.zig");
const utils = @import("../utils.zig");
const Allocation = @import("Allocation.zig");
const VulkanEngine = @import("../VulkanEngine.zig");

const logger = utils.Logger(.Image);
const scoped = utils.Scoped(logger);
const check2 = scoped.check2;
const check = scoped.check;

const Image = @This();

const Device = VulkanEngine.Device;
const VkImage = c.VkImage;
const VkImageView = c.VkImageView;


pub fn calculate_mip_levels(w: u32, h: u32) u32 {
    return std.math.log2(@max(w, h)) + 1;
}

image: VkImage,
image_view: ?VkImageView,

extent: c.VkExtent3D,
format: c.VkFormat,
mip_levels: u32,
layout: c.VkImageLayout,

allocation: ?Allocation,
device: ?Device,

fn init(alloc_info: c.VmaAllocationCreateInfo, create_info: c.VkImageCreateInfo) Image {
    var image: c.VkImage = undefined;
    var allocation: c.VmaAllocation = undefined;

    check2(c.vmaCreateImage(VulkanEngine.vma_allocator, &create_info, &alloc_info, &image, &allocation, null), "failed to create an image");
    return Image {
        .image = image,
        .image_view = null,
        .allocation = Allocation.init(allocation, false),
        .format = create_info.format,
        .mip_levels = create_info.mipLevels,
        .extent = create_info.extent,
        .device = null,
        .layout = create_info.initialLayout
    };
}

pub fn from(image: c.VkImage, extent: c.VkExtent3D, format: c.VkFormat, mip_levels: u32, layout: c.VkImageLayout) Image {
    return Image {
        .image = image,
        .extent = extent,
        .format = format,
        .mip_levels = mip_levels,
        .allocation = null,
        .device = null,
        .image_view = null,
        .layout = layout
    };
}

pub const ImageError = error {
    UnknownError
};

pub fn load(device: Device, file: []const u8, enable_mipmaps: bool) !Image {
    var _width: i32 = 0;
    var _height: i32 = 0;
    var channels: i32 = 0;
    const pixels: ?*c.stbi_uc = c.stbi_load(@ptrCast(file), &_width, &_height, &channels, c.STBI_rgb_alpha);

    const tex_width: u32 = @intCast(_width);
    const tex_height: u32 = @intCast(_height);
    const image_size: c.VkDeviceSize = tex_width * tex_height * 4;

    const tex_mip_levels = calculate_mip_levels(tex_width, tex_height);

    if(pixels == null) {
        // possibly file not found
        return ImageError.UnknownError;
    }

    var staging_buffer = VulkanEngine.Buffer.create_staging_buffer(image_size, pixels);
    defer staging_buffer.deinit();

    c.stbi_image_free(pixels);

    var image = Builder
        .builder3(tex_width, tex_height)
        .with_format(c.VK_FORMAT_R8G8B8A8_SRGB)
        .with_mip_levels(tex_mip_levels)
        .with_usage(c.VK_IMAGE_USAGE_TRANSFER_SRC_BIT | c.VK_IMAGE_USAGE_TRANSFER_DST_BIT | c.VK_IMAGE_USAGE_SAMPLED_BIT)
        .with_vma_usage(c.VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
        .build();
    image.create_image_view(device, c.VK_IMAGE_ASPECT_COLOR_BIT);

    image.transition_image_layout(c.VK_IMAGE_LAYOUT_UNDEFINED, c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    staging_buffer.copy_to_image(image);

    if(enable_mipmaps)
        image.generate_mipmaps(device);

    image.transition_image_layout(c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, c.VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

    return image;
}


pub fn deinit(self: *Image) void {
    if(self.image_view) |image_view| {
        c.vkDestroyImageView(self.device.?.get_handle(), image_view, VulkanEngine.api_allocator);
        self.image_view = null;
    }
    if(self.allocation) |allocation| {
        c.vmaDestroyImage(VulkanEngine.vma_allocator, self.image, allocation.get_handle());
        self.image = null;
        self.allocation = null;
    }
    self.* = undefined;
}

pub fn get_handle(self: Image) VkImage {
    return self.image;
}

pub fn get_view_handle(self: Image) VkImageView {
    return self.image_view.?;
}

pub fn create_image_view(self: *Image, device: Device, aspect_flags: c.VkImageAspectFlags) void {
    self.device = device;

    const view_info: c.VkImageViewCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .image = self.get_handle(),
        .viewType = c.VK_IMAGE_VIEW_TYPE_2D,
        .format = self.format,
        .subresourceRange = .{
            .aspectMask = aspect_flags,
            .baseMipLevel = 0,
            .levelCount = self.mip_levels,
            .baseArrayLayer = 0,
            .layerCount = 1
        }
    };
    var image_view: c.VkImageView = undefined;
    check2(c.vkCreateImageView(device.get_handle(), &view_info, VulkanEngine.api_allocator, &image_view), "failed to create image view");
    self.image_view = image_view;
}

pub fn width(self: Image) u32 {
    return self.extent.width;
}

pub fn height(self: Image) u32 {
    return self.extent.height;
}

// TODO: remove old_layout parameter
pub fn transition_image_layout(self: *Image, comptime old_layout: c.VkImageLayout, comptime new_layout: c.VkImageLayout) void {
    std.debug.assert(self.layout == old_layout);
    const command_buffer = VulkanEngine.begin_single_time_commands();
    defer VulkanEngine.end_single_time_commands(command_buffer);

    var source_stage: c.VkPipelineStageFlags = undefined;
    var destination_stage: c.VkPipelineStageFlags = undefined;
    var src_access_mask: u32 = 0;
    var dst_access_mask: u32 = 0;

    if(old_layout == c.VK_IMAGE_LAYOUT_UNDEFINED and new_layout == c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
        dst_access_mask = c.VK_ACCESS_TRANSFER_WRITE_BIT;

        source_stage = c.VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destination_stage = c.VK_PIPELINE_STAGE_TRANSFER_BIT;
    } else if(old_layout == c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL and new_layout == c.VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
        src_access_mask = c.VK_ACCESS_TRANSFER_WRITE_BIT;
        dst_access_mask = c.VK_ACCESS_SHADER_READ_BIT;

        source_stage = c.VK_PIPELINE_STAGE_TRANSFER_BIT;
        destination_stage = c.VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    } else {
        @compileError("unsupported layout transition");
    }

    const barrier: c.VkImageMemoryBarrier = .{
        .sType = c.VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .oldLayout = old_layout,
        .newLayout = new_layout,
        .srcQueueFamilyIndex = c.VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = c.VK_QUEUE_FAMILY_IGNORED,
        .image = self.get_handle(),
        .subresourceRange = .{
            .aspectMask = c.VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = self.mip_levels,
            .baseArrayLayer = 0,
            .layerCount = 1
        },
        .srcAccessMask = src_access_mask,
        .dstAccessMask = dst_access_mask
    };

    c.vkCmdPipelineBarrier(command_buffer, source_stage, destination_stage, 0, 0, null, 0, null, 1, &barrier);
    self.layout = new_layout;
}


// TODO: maybe automatically transition image layout
pub fn generate_mipmaps(self: Image, device: Device) void {
    std.debug.assert(self.layout == c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

    var format_props: c.VkFormatProperties = undefined;
    c.vkGetPhysicalDeviceFormatProperties(device.gpu.get_handle(), self.format, &format_props);

    if((format_props.optimalTilingFeatures & c.VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT) == 0) {
        std.debug.panic("texture image format does not support linear blitting", .{});
    }

    const command_buffer = VulkanEngine.begin_single_time_commands();
    defer VulkanEngine.end_single_time_commands(command_buffer);

    var barrier: c.VkImageMemoryBarrier = .{
        .sType = c.VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .image = self.get_handle(),
        .srcQueueFamilyIndex = c.VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = c.VK_QUEUE_FAMILY_IGNORED,
        .subresourceRange = .{
            .aspectMask = c.VK_IMAGE_ASPECT_COLOR_BIT,
            .baseArrayLayer = 0,
            .layerCount = 1,
            .levelCount = 1
        },
        .oldLayout = c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        .newLayout = c.VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        .srcAccessMask = c.VK_ACCESS_TRANSFER_WRITE_BIT,
        .dstAccessMask = c.VK_ACCESS_TRANSFER_READ_BIT
    };

    var mip_width = self.width();
    var mip_height = self.height();
    for(1..self.mip_levels) |_i| {
        const i = @as(u32, @intCast(_i));
        barrier.subresourceRange.baseMipLevel = i - 1;
        barrier.oldLayout = c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        barrier.newLayout = c.VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
        barrier.srcAccessMask = c.VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = c.VK_ACCESS_TRANSFER_READ_BIT;

        c.vkCmdPipelineBarrier(
            command_buffer,
            c.VK_PIPELINE_STAGE_TRANSFER_BIT,
            c.VK_PIPELINE_STAGE_TRANSFER_BIT,
            0,
            0,
            null,
            0,
            null,
            1,
            &barrier
        );

        const blit: c.VkImageBlit = .{
            .srcOffsets = [2]c.VkOffset3D{ 
                .{ .x = 0, .y = 0, .z = 0 },
                .{ .x = @as(i32, @intCast(mip_width)), .y = @as(i32, @intCast(mip_height)), .z = 1 } },
            .srcSubresource = .{
                .aspectMask = c.VK_IMAGE_ASPECT_COLOR_BIT,
                .mipLevel = i - 1,
                .baseArrayLayer = 0,
                .layerCount = 1,
            },
            .dstOffsets = [2]c.VkOffset3D {
                .{ .x = 0, .y = 0, .z = 0},
                .{ 
                    .x = if(mip_width > 1) @as(i32, @intCast(mip_width / 2)) else 1, 
                    .y = if(mip_height > 1) @as(i32, @intCast(mip_height / 2)) else 1, 
                    .z = 1 
                } 
            },
            .dstSubresource = .{
                .aspectMask = c.VK_IMAGE_ASPECT_COLOR_BIT,
                .mipLevel = i,
                .baseArrayLayer = 0,
                .layerCount = 1
            }
        };

        c.vkCmdBlitImage(
            command_buffer,
            self.get_handle(),
            c.VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            self.get_handle(),
            c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            1,
            &blit,
            c.VK_FILTER_LINEAR
        );

        barrier.oldLayout = c.VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
        //barrier.newLayout = c.VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        barrier.newLayout = c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        barrier.srcAccessMask = c.VK_ACCESS_TRANSFER_READ_BIT;
        barrier.dstAccessMask = c.VK_ACCESS_SHADER_READ_BIT;

        c.vkCmdPipelineBarrier(command_buffer,
            c.VK_PIPELINE_STAGE_TRANSFER_BIT, c.VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
            0, null,
            0, null,
            1, &barrier
        );

        if(mip_width > 1) mip_width /= 2;
        if(mip_height > 1) mip_height /= 2;
    }

    barrier.subresourceRange.baseMipLevel = self.mip_levels - 1;
    barrier.oldLayout = c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    //barrier.newLayout = c.VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    barrier.newLayout = c.VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    barrier.srcAccessMask = c.VK_ACCESS_TRANSFER_WRITE_BIT;
    barrier.dstAccessMask = c.VK_ACCESS_SHADER_READ_BIT;

    c.vkCmdPipelineBarrier(command_buffer,
        c.VK_PIPELINE_STAGE_TRANSFER_BIT, c.VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
        0, null,
        0, null,
        1, &barrier);
}


pub const Texture = struct {
    image: Image,
    sampler: *c.VkSampler,

    pub fn init(device: Device, file: []const u8, enable_mipmaps: bool, sampler: *c.VkSampler) !Texture {
        return Texture {
            .image = try Image.load(device, file, enable_mipmaps),
            .sampler = sampler
        };
    }

    pub fn deinit(self: *Texture) void {
        self.image.deinit();
        
        self.* = undefined;
    }
};


pub const Builder = struct {
    alloc_info: c.VmaAllocationCreateInfo,
    create_info: c.VkImageCreateInfo,

    pub fn builder(extent: c.VkExtent3D) Builder {
        return .{
            .alloc_info = c.VmaAllocationCreateInfo {
                .usage = c.VMA_MEMORY_USAGE_AUTO
            },
            .create_info = c.VkImageCreateInfo {
                .sType = c.VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
                .pNext = null,
                .extent = extent,
                .arrayLayers = 1,
                .mipLevels = 1,
                .format = c.VK_FORMAT_R8G8B8A8_UNORM,
                .samples = c.VK_SAMPLE_COUNT_1_BIT,
                .tiling = c.VK_IMAGE_TILING_OPTIMAL,
                .imageType = c.VK_IMAGE_TYPE_2D,
                .initialLayout = c.VK_IMAGE_LAYOUT_UNDEFINED
            }
        };
    }

    pub fn builder2(extent: c.VkExtent2D) Builder {
        return builder(.{
            .width = extent.width,
            .height = extent.height,
            .depth = 1
        });
    }

    pub fn builder3(_width: u32, _height: u32) Builder {
        return builder(.{
            .width = _width,
            .height = _height,
            .depth = 1
        });
    }

    pub fn build(self: Builder) Image {
        return init(self.alloc_info, self.create_info);
    }

    pub fn with_format(self: Builder, format: c.VkFormat) Builder {
        var _builder = self;
        _builder.create_info.format = format;
        return _builder;
    }

    pub fn with_usage(self: Builder, usage: c.VkImageUsageFlags) Builder {
        var _builder = self;
        _builder.create_info.usage = usage;
        return _builder;
    }
    
    pub fn with_sharing_mode(self: Builder, sharing: c.VkSharingMode) Builder {
        var _builder = self;
        _builder.create_info.sharingMode = sharing;
        return _builder;
    }
    
    pub fn with_flags(self: Builder, flags: c.VkImageCreateFlags) Builder {
        var _builder = self;
        _builder.create_info.flags = flags;
        return _builder;
    }

    pub fn with_image_type(self: Builder, ty: c.VkImageType) Builder {
        var _builder = self;
        _builder.create_info.imageType = ty;
        return _builder;
    }

    pub fn with_mip_levels(self: Builder, levels: u32) Builder {
        var _builder = self;
        _builder.create_info.mipLevels = levels;
        return _builder;
    }

    pub fn with_sample_count(self: Builder, sample_count: c.VkSampleCountFlagBits) Builder {
        var _builder = self;
        _builder.create_info.samples = sample_count;
        return _builder;
    }

    pub fn with_tiling(self: Builder, tiling: c.VkImageTiling) Builder {
        var _builder = self;
        _builder.create_info.tiling = tiling;
        return _builder;
    }

    pub fn with_layout(self: Builder, layout: c.VkImageLayout) Builder {
        var _builder = self;
        _builder.create_info.initialLayout = layout;
        return _builder;
    }


    pub fn with_vma_usage(self: Builder, usage: c.VmaMemoryUsage) Builder {
        var _builder = self;
        _builder.alloc_info.usage = usage;
        return _builder;
    }

    pub fn with_vma_flags(self: Builder, flags: c.VmaAllocationCreateFlags) Builder {
        var _builder = self;
        _builder.alloc_info.flags = flags;
        return _builder;
    }
};

const std = @import("std");
const c = @import("../c.zig");
const utils = @import("../utils.zig");
const Allocation = @import("Allocation.zig");
const VulkanEngine = @import("../VulkanEngine.zig");

const logger = utils.Logger(.Device);
const scoped = utils.Scoped(logger);
const check2 = scoped.check2;
const check = scoped.check;
const vk_get_properties = scoped.vk_get_properties;

const Device = @This();
const PhysicalDevice = VulkanEngine.PhysicalDevice;
const Vertex = VulkanEngine.Vertex;
const InstanceData = VulkanEngine.InstanceData;

const VkDevice = c.VkDevice;
const VkQueue = c.VkQueue;

device: VkDevice,
graphics_queue: ?VkQueue,
present_queue: ?VkQueue,
gpu: PhysicalDevice,

const DEVICE_EXTENSIONS = [_][]const u8 { c.VK_KHR_SWAPCHAIN_EXTENSION_NAME };

pub fn init(allocator: std.mem.Allocator, gpu: PhysicalDevice) !Device {
    const indices = gpu.find_queue_families(allocator);
    const unique_queue_families = [_]u32 { indices.graphics_family.?, indices.present_family.? };
    var unique_queue_create_infos: [unique_queue_families.len] c.VkDeviceQueueCreateInfo = undefined;
    var seen = [_] ?u32 { null } ** unique_queue_families.len;

    const queue_priority: f32 = 1.0;
    var j: u32 = 0;
    for(unique_queue_families) |family| {
        // family ids must be unique
        var already_seen = false;
        for(seen) |seen_family| {
            if(seen_family) |s_family| {
                if(s_family == family) {
                    already_seen = true;
                }
            }
        }
        if(already_seen) continue;
        unique_queue_create_infos[j] = .{
            .sType = c.VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            .queueFamilyIndex = family,
            .queueCount = 1,
            .pQueuePriorities = &queue_priority
        };
        seen[j] = family;
        j += 1;
    }
    const device_features: c.VkPhysicalDeviceFeatures = .{
        .samplerAnisotropy = c.VK_TRUE,
        .sampleRateShading = c.VK_TRUE
    };
    const create_info: c.VkDeviceCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        .pQueueCreateInfos = &unique_queue_create_infos,
        .queueCreateInfoCount = j,
        .pEnabledFeatures = &device_features,
        .enabledExtensionCount = DEVICE_EXTENSIONS.len,
        .ppEnabledExtensionNames = @ptrCast(&DEVICE_EXTENSIONS),
        .enabledLayerCount = if(utils.ENABLE_VALIDATION_LAYERS) VulkanEngine.Instance.VALIDATION_LAYERS.len else 0,
        .ppEnabledLayerNames = if(utils.ENABLE_VALIDATION_LAYERS) @ptrCast(&VulkanEngine.Instance.VALIDATION_LAYERS) else null
    };
    var device: VkDevice = undefined;
    try VulkanEngine.emit_error(c.vkCreateDevice(gpu.get_handle(), &create_info, VulkanEngine.api_allocator, &device));

    var graphics_queue: VkQueue = undefined;
    var present_queue: VkQueue = undefined;
    c.vkGetDeviceQueue(device, indices.graphics_family.?, 0, &graphics_queue);
    c.vkGetDeviceQueue(device, indices.present_family.?, 0, &present_queue);

    return Device {
        .device = device,
        .present_queue = present_queue,
        .graphics_queue = graphics_queue,
        .gpu = gpu
    };
}

pub fn deinit(self: *Device) void {
    self.graphics_queue = null;
    self.present_queue = null;

    c.vkDestroyDevice(self.device, VulkanEngine.api_allocator);

    self.* = undefined;
}

pub fn get_handle(self: Device) VkDevice {
    return self.device;
}

pub fn get_gpu(self: Device) PhysicalDevice {
    return self.gpu;
}

pub fn create_descriptor_set_layout(self: Device) !c.VkDescriptorSetLayout {
    const ubo_layout_binding: c.VkDescriptorSetLayoutBinding = .{
        .binding = 0,
        .descriptorType = c.VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .descriptorCount = 1,
        .stageFlags = c.VK_SHADER_STAGE_VERTEX_BIT
    };

    const sampler_layout_binding: c.VkDescriptorSetLayoutBinding = .{
        .binding = 1,
        .descriptorCount = 1,
        .descriptorType = c.VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .pImmutableSamplers = null,
        .stageFlags = c.VK_SHADER_STAGE_FRAGMENT_BIT
    };

    const bindings = [_]c.VkDescriptorSetLayoutBinding { ubo_layout_binding, sampler_layout_binding };

    const layout_info: c.VkDescriptorSetLayoutCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .bindingCount = bindings.len,
        .pBindings = &bindings
    };

    var descriptor_set_layout: c.VkDescriptorSetLayout = undefined;
    try VulkanEngine.emit_error(c.vkCreateDescriptorSetLayout(self.get_handle(), &layout_info, VulkanEngine.api_allocator, &descriptor_set_layout));
    return descriptor_set_layout;
}

pub fn create_descriptor_pool(self: Device) c.VkDescriptorPool {
    const pool_sizes = [_]c.VkDescriptorPoolSize {
        .{
            .type = c.VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .descriptorCount = utils.MAX_FRAMES_IN_FLIGHT
        },
        .{
            .type = c.VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .descriptorCount = utils.MAX_FRAMES_IN_FLIGHT
        }
    };
    const pool_info: c.VkDescriptorPoolCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .poolSizeCount = pool_sizes.len,
        .pPoolSizes = &pool_sizes,
        .maxSets = utils.MAX_FRAMES_IN_FLIGHT,
    };
    var descriptor_pool: c.VkDescriptorPool = undefined;
    check(c.vkCreateDescriptorPool(self.get_handle(), &pool_info, VulkanEngine.api_allocator, &descriptor_pool));
    return descriptor_pool;
}

pub fn create_descriptor_sets(self: Device, descriptor_set_layout: c.VkDescriptorSetLayout, descriptor_pool: c.VkDescriptorPool, texture_image: VulkanEngine.Image.Texture, uniform_buffers: [utils.MAX_FRAMES_IN_FLIGHT]VulkanEngine.Buffer) [utils.MAX_FRAMES_IN_FLIGHT]c.VkDescriptorSet {
    var layouts = [_]c.VkDescriptorSetLayout { descriptor_set_layout } ** utils.MAX_FRAMES_IN_FLIGHT;
    const alloc_info: c.VkDescriptorSetAllocateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .descriptorPool = descriptor_pool,
        .descriptorSetCount = utils.MAX_FRAMES_IN_FLIGHT,
        .pSetLayouts = &layouts
    };
    var descriptor_sets: [utils.MAX_FRAMES_IN_FLIGHT]c.VkDescriptorSet = undefined;
    check(c.vkAllocateDescriptorSets(self.get_handle(), &alloc_info, &descriptor_sets));

    for(0..utils.MAX_FRAMES_IN_FLIGHT) |i| {
        const buffer_info: c.VkDescriptorBufferInfo = .{
            .buffer = uniform_buffers[i].get_handle(),
            .offset = 0,
            .range = @sizeOf(VulkanEngine.UniformBufferObject)
        };
        const image_info: c.VkDescriptorImageInfo = .{
            .imageLayout = c.VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
            .imageView = texture_image.image.get_view_handle(),
            .sampler = texture_image.sampler.*
        };

        const descriptor_writes = [_]c.VkWriteDescriptorSet {
            .{
                .sType = c.VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .dstSet = descriptor_sets[i],
                .dstBinding = 0,
                .dstArrayElement = 0,
                .descriptorType = c.VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                .descriptorCount = 1,
                .pBufferInfo = &buffer_info
            },
            .{
                .sType = c.VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .dstSet = descriptor_sets[i],
                .dstBinding = 1,
                .dstArrayElement = 0,
                .descriptorType = c.VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                .descriptorCount = 1,
                .pImageInfo = &image_info
            }
        };
        c.vkUpdateDescriptorSets(self.get_handle(), descriptor_writes.len, &descriptor_writes, 0, null);
    }
    return descriptor_sets;
}

const Pipeline = struct {
    pipeline: c.VkPipeline,
    layout: c.VkPipelineLayout
};

pub fn create_graphics_pipeline(self: Device, allocator: std.mem.Allocator, render_pass: c.VkRenderPass, descriptor_set_layout: c.VkDescriptorSetLayout) !Pipeline {
    const vert_shader_code = try read_shader(allocator, "vert.spv");
    const frag_shader_code = try read_shader(allocator, "frag.spv");
    const vert_shader_module = self.create_shader_module(vert_shader_code);
    const frag_shader_module = self.create_shader_module(frag_shader_code);

    defer c.vkDestroyShaderModule(self.get_handle(), vert_shader_module, VulkanEngine.api_allocator);
    defer c.vkDestroyShaderModule(self.get_handle(), frag_shader_module, VulkanEngine.api_allocator);
    defer allocator.free(vert_shader_code);
    defer allocator.free(frag_shader_code);

    const vert_shader_stage_info: c.VkPipelineShaderStageCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = c.VK_SHADER_STAGE_VERTEX_BIT,
        .module = vert_shader_module,
        .pName = "main"
    };
    const frag_shader_stage_info: c.VkPipelineShaderStageCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = c.VK_SHADER_STAGE_FRAGMENT_BIT,
        .module = frag_shader_module,
        .pName = "main"
    };

    const shader_stages = [_]c.VkPipelineShaderStageCreateInfo {vert_shader_stage_info, frag_shader_stage_info};

    const dynamic_states = [_]c.VkDynamicState {
        c.VK_DYNAMIC_STATE_VIEWPORT,
        c.VK_DYNAMIC_STATE_SCISSOR
    };

    const dynamic_state: c.VkPipelineDynamicStateCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        .dynamicStateCount = dynamic_states.len,
        .pDynamicStates = &dynamic_states
    };

    const binding_desc = [_]c.VkVertexInputBindingDescription {
        Vertex.get_binding_description(),
        .{
            .binding = 1,
            .stride = @sizeOf(InstanceData),
            .inputRate = c.VK_VERTEX_INPUT_RATE_INSTANCE
        }
    };
    const attribute_desc = Vertex.get_attribute_descriptions() ++ [_]c.VkVertexInputAttributeDescription {
        .{
            .binding = 1,
            .location = 3,
            .format = c.VK_FORMAT_R32G32B32_SFLOAT,
            .offset = 0
        }
    };

    const vertex_input_info: c.VkPipelineVertexInputStateCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .vertexBindingDescriptionCount = @as(u32, @intCast(binding_desc.len)),
        .pVertexBindingDescriptions = &binding_desc,
        .vertexAttributeDescriptionCount = @as(u32, @intCast(attribute_desc.len)),
        .pVertexAttributeDescriptions = &attribute_desc 
    };

    const input_assembly: c.VkPipelineInputAssemblyStateCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .topology = c.VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
        .primitiveRestartEnable = c.VK_FALSE
    };

    const viewport_state: c.VkPipelineViewportStateCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .viewportCount = 1,
        .scissorCount = 1
    };

    const rasterizer: c.VkPipelineRasterizationStateCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .depthClampEnable = c.VK_FALSE,
        .rasterizerDiscardEnable = c.VK_FALSE,
        .polygonMode = c.VK_POLYGON_MODE_FILL,
        .lineWidth = 1.0,
        .cullMode = c.VK_CULL_MODE_BACK_BIT,
        .frontFace = c.VK_FRONT_FACE_CLOCKWISE,
        .depthBiasEnable = c.VK_FALSE
    };

    // WARNING: big impact on performance if sample shading is enabled
    // Quick testing showed that performance can actually be higher with
    // sample shading enabled and msaa_samples <= 4 that without
    // higher minSampleShading also have a significant fps boost
    const msaa_samples = self.gpu.get_sample_counts().max();
    const multisampling: c.VkPipelineMultisampleStateCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .sampleShadingEnable = c.VK_FALSE,
        //.minSampleShading = 0.2,
        .rasterizationSamples = msaa_samples,
    };

    const color_blend_attachment: c.VkPipelineColorBlendAttachmentState = .{
        .colorWriteMask = c.VK_COLOR_COMPONENT_R_BIT | c.VK_COLOR_COMPONENT_G_BIT | c.VK_COLOR_COMPONENT_B_BIT | c.VK_COLOR_COMPONENT_A_BIT,
        .blendEnable = c.VK_FALSE
    };

    const color_blending: c.VkPipelineColorBlendStateCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .logicOpEnable = c.VK_FALSE,
        .attachmentCount = 1,
        .pAttachments = &color_blend_attachment,
        .logicOp = c.VK_LOGIC_OP_COPY
    };

    const pipeline_layout_info: c.VkPipelineLayoutCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .setLayoutCount = 1,
        .pSetLayouts = &descriptor_set_layout
    };

    var pipeline_layout: c.VkPipelineLayout = undefined;
    check(c.vkCreatePipelineLayout(self.get_handle(), &pipeline_layout_info, VulkanEngine.api_allocator, &pipeline_layout));

    const depth_stencil: c.VkPipelineDepthStencilStateCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .depthTestEnable = c.VK_TRUE,
        .depthWriteEnable = c.VK_TRUE,
        .depthCompareOp = c.VK_COMPARE_OP_LESS,
        .depthBoundsTestEnable = c.VK_FALSE,
        .stencilTestEnable = c.VK_FALSE
    };

    const pipeline_info: c.VkGraphicsPipelineCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
        .stageCount = 2,
        .pStages = &shader_stages,
        .pVertexInputState = &vertex_input_info,
        .pInputAssemblyState = &input_assembly,
        .pViewportState = &viewport_state,
        .pRasterizationState = &rasterizer,
        .pMultisampleState = &multisampling,
        .pColorBlendState = &color_blending,
        .pDynamicState = &dynamic_state,
        .pDepthStencilState = &depth_stencil,
        .layout = pipeline_layout,
        .renderPass = render_pass,
        .subpass = 0
    };

    var graphics_pipeline: c.VkPipeline = undefined;
    try VulkanEngine.emit_error(c.vkCreateGraphicsPipelines(self.get_handle(), null, 1, &pipeline_info, VulkanEngine.api_allocator, &graphics_pipeline));
    return Pipeline {
        .pipeline = graphics_pipeline,
        .layout = pipeline_layout
    };
}


pub fn create_command_pool(self: Device, allocator: std.mem.Allocator) !c.VkCommandPool {
    const queue_family_indices = self.gpu.find_queue_families(allocator);

    const pool_info: c.VkCommandPoolCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .flags = c.VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
        .queueFamilyIndex = queue_family_indices.graphics_family.?
    };

    var command_pool: c.VkCommandPool = undefined;
    try VulkanEngine.emit_error(c.vkCreateCommandPool(self.get_handle(), &pool_info, VulkanEngine.api_allocator, &command_pool));
    return command_pool;
}

pub fn create_texture_sampler(self: Device) c.VkSampler {

    const sampler_info: c.VkSamplerCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .magFilter = c.VK_FILTER_LINEAR,
        .minFilter = c.VK_FILTER_LINEAR,
        .addressModeU = c.VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .addressModeV = c.VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .addressModeW = c.VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .anisotropyEnable = c.VK_TRUE,
        .maxAnisotropy = self.gpu.properties.limits.maxSamplerAnisotropy,
        .borderColor = c.VK_BORDER_COLOR_INT_OPAQUE_BLACK,
        .unnormalizedCoordinates = c.VK_FALSE,
        .compareEnable = c.VK_FALSE,
        .compareOp = c.VK_COMPARE_OP_ALWAYS,
        .mipmapMode = c.VK_SAMPLER_MIPMAP_MODE_LINEAR,
        .mipLodBias = 0.0,
        .minLod = 0.0,
        .maxLod = c.VK_LOD_CLAMP_NONE
    };

    var texture_sampler: c.VkSampler = undefined;
    check2(c.vkCreateSampler(self.get_handle(), &sampler_info, VulkanEngine.api_allocator, &texture_sampler), "failed to create texture sampler");
    return texture_sampler;
}









fn read_shader(allocator: std.mem.Allocator, filename: []const u8) ![]u8 {
    const dir = try std.fs.cwd().openDir("src/shaders", .{});
    return try dir.readFileAlloc(allocator, filename, std.math.maxInt(usize));
}


fn create_shader_module(self: Device, code: []u8) c.VkShaderModule {
    const create_info: c.VkShaderModuleCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        .codeSize = code.len,
        .pCode = @alignCast(@ptrCast(code))//std.mem.bytesAsSlice(u32, code)//@ptrCast(code)
    };
    var shader_module: c.VkShaderModule = undefined;
    check(c.vkCreateShaderModule(self.get_handle(), &create_info, VulkanEngine.api_allocator, &shader_module));
    return shader_module;
}

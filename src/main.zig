const std = @import("std");
const c = @import("c.zig");
const utils = @import("utils.zig");
const tracy = @import("tracy.zig");
const Camera = @import("Camera.zig");

const VulkanEngine = @import("VulkanEngine.zig");
const Image = VulkanEngine.Image;
const Buffer = VulkanEngine.Buffer;
const Instance = VulkanEngine.Instance;
const GlfwWindow = VulkanEngine.GlfwWindow;

const ImageBuilder = Image.Builder;
const BufferBuilder = Buffer.Builder;

pub const std_options = utils.std_options;


const logger = utils.Logger(.default);
const scoped = utils.Scoped(logger);
const check = scoped.check;
const check2 = scoped.check2;
const vk_get_properties = scoped.vk_get_properties;



var gpa = std.heap.GeneralPurposeAllocator(.{}){};
const _allocator = gpa.allocator();
var tracy_alloc = tracy.TracyAllocator(null).init(_allocator);
const allocator = tracy_alloc.allocator();


var command_buffers: [utils.MAX_FRAMES_IN_FLIGHT]c.VkCommandBuffer = undefined;
var image_available_semas: [utils.MAX_FRAMES_IN_FLIGHT]c.VkSemaphore = undefined;
var render_finished_semas: [utils.MAX_FRAMES_IN_FLIGHT]c.VkSemaphore = undefined;
var in_flight_fences: [utils.MAX_FRAMES_IN_FLIGHT]c.VkFence = undefined;
var current_frame: usize = 0;
var frame_buffer_resized: bool = false;
var vertex_buffer: Buffer = undefined;
var index_buffer: Buffer = undefined;
// TODO: one array of desc sets per pipeline
var descriptor_sets: [utils.MAX_FRAMES_IN_FLIGHT]c.VkDescriptorSet = undefined;

var instance_buffer: Buffer = undefined;

var vertices: []VulkanEngine.Vertex = undefined;
var indices: []u32 = undefined;

var start_time: std.time.Timer = undefined;

var fences_name: [utils.MAX_FRAMES_IN_FLIGHT][:0]u8 = undefined;
var vk_ctx: [utils.MAX_FRAMES_IN_FLIGHT]tracy.VkCtx = undefined;

var focused: bool = true;
var camera: Camera = undefined;

var engine: VulkanEngine = undefined;


const InputInfo = struct {
    mouse: MouseInfo,
    key: KeyInfo
};

const KeyInfo = struct {
    left: bool,
    right: bool,
    up: bool,
    down: bool
};

const MouseInfo = struct {
    left_pressed: bool,
    right_pressed: bool,
    middle_pressed: bool,
    x: f64,
    y: f64,
    dx: f64,
    dy: f64
};

var input_info = InputInfo {
    .mouse = .{
        .left_pressed = false,
        .right_pressed = false,
        .middle_pressed = false,
        .x = 0,
        .y = 0,
        .dx = 0,
        .dy = 0
    },
    .key = .{
        .up = false,
        .left = false,
        .down = false,
        .right = false
    }
};

pub fn main() !void {
    start_time = try std.time.Timer.start();
    camera = Camera.init(std.math.pi / 4.0, 1.0, 0.1, 20.0);
    camera.set_position(c.vec3 { 0.0, 0.0, 2.0 });

    
    engine = try VulkanEngine.init(allocator, "Voxels", true);
    defer engine.deinit();

    cleanup();
}





fn update_camera(dt: f64) void {
    if(input_info.key.up) {
        var direction = camera.get_front();
        c.glmc_vec3_scale(&direction, @floatCast(Camera.translation_speed * dt), &direction);
        camera.translate(direction);
    }
    if(input_info.key.down) {
        var direction = camera.get_back();
        c.glmc_vec3_scale(&direction, @floatCast(Camera.translation_speed * dt), &direction);
        camera.translate(direction);
    }
    if(input_info.key.left) {
        var direction = camera.get_left();
        c.glmc_vec3_scale(&direction, @floatCast(Camera.translation_speed * dt), &direction);
        camera.translate(direction);
    }
    if(input_info.key.right) {
        var direction = camera.get_right();
        c.glmc_vec3_scale(&direction, @floatCast(Camera.translation_speed * dt), &direction);
        camera.translate(direction);
    }
}



pub fn main_loop(uniform_buffers: *[utils.MAX_FRAMES_IN_FLIGHT]Buffer) void {
    var frame_count: u32 = 0;
    var frame_time: f64 = 0;
    var timer = std.time.Timer.start() catch unreachable;

    var frame_timer = std.time.Timer.start() catch unreachable;
    while(c.glfwWindowShouldClose(window) == 0) {
        c.glfwPollEvents();
        //if(!focused) continue;

        // in seconds
        const dt: f64 = @as(f64, @floatFromInt(@divFloor(frame_timer.lap(), std.time.ns_per_us))) / std.time.us_per_s;
        draw_frame(uniform_buffers) catch unreachable;
        update_camera(dt);
        frame_count += 1;
        frame_time += dt;

        const elapsed = @as(f64, @floatFromInt(timer.read() / 1000000)) / 1000;

        if(elapsed > 0.5) {
            const fps = @as(f64, @floatFromInt(frame_count)) / elapsed;
            const av_frame_time = frame_time * 1000 / @as(f64, @floatFromInt(frame_count));
            logger.debug("FPS: {d:.1}, av frame time: {d:.1} ms", .{fps, av_frame_time});
            frame_count = 0;
            frame_time = 0;
            timer.reset();
        }
    }
    check(c.vkDeviceWaitIdle(device.get_handle()));

    for(0..utils.MAX_FRAMES_IN_FLIGHT) |i| {
        vk_ctx[i].end();
    }
}

fn cleanup() void {
    vertex_buffer.deinit();
    instance_buffer.deinit();
    index_buffer.deinit();
    for(0..utils.MAX_FRAMES_IN_FLIGHT) |i| {
        c.vkDestroySemaphore(device.get_handle(), image_available_semas[i], VulkanEngine.api_allocator);
        c.vkDestroySemaphore(device.get_handle(), render_finished_semas[i], VulkanEngine.api_allocator);
        c.vkDestroyFence(device.get_handle(), in_flight_fences[i], VulkanEngine.api_allocator);
    }
}



pub fn load_cube() !void {
    vertices = try allocator.alloc(VulkanEngine.Vertex, 8);
    indices = try allocator.alloc(u32, 18);

    //for(0..2) |i| {
    //    for(0..2) |j| {
    //        for(0..2) |k| {
    //            vertices[4*i + 2*j + k] = .{
    //                .pos = [_]f32 {
    //            };
    //        }
    //    }
    //}
    vertices = @constCast(&[_]VulkanEngine.Vertex {
        // down
        .{ .pos = [_]f32 { 1.0,  1.0, -1.0}, .color = [_]f32 {1.0, 0.0, 0.0}, .tex_coord = [_]f32 {0.0, 0.0} },
        .{ .pos = [_]f32 {-1.0,  1.0, -1.0}, .color = [_]f32 {0.0, 1.0, 0.0}, .tex_coord = [_]f32 {1.0, 0.0} },
        .{ .pos = [_]f32 {-1.0, -1.0, -1.0}, .color = [_]f32 {0.0, 0.0, 1.0}, .tex_coord = [_]f32 {1.0, 1.0} },
        .{ .pos = [_]f32 { 1.0, -1.0, -1.0}, .color = [_]f32 {0.0, 0.0, 1.0}, .tex_coord = [_]f32 {0.0, 1.0} },

        // up
        .{ .pos = [_]f32 { 1.0, -1.0,  1.0}, .color = [_]f32 {1.0, 0.0, 0.0}, .tex_coord = [_]f32 {1.0, 1.0} },
        .{ .pos = [_]f32 {-1.0, -1.0,  1.0}, .color = [_]f32 {0.0, 1.0, 0.0}, .tex_coord = [_]f32 {0.0, 1.0} },
        .{ .pos = [_]f32 {-1.0,  1.0,  1.0}, .color = [_]f32 {0.0, 0.0, 1.0}, .tex_coord = [_]f32 {0.0, 0.0} },
        .{ .pos = [_]f32 { 1.0,  1.0,  1.0}, .color = [_]f32 {0.0, 0.0, 1.0}, .tex_coord = [_]f32 {1.0, 0.0} },

        // -x
        .{ .pos = [_]f32 {-1.0,  1.0,  1.0}, .color = [_]f32 {1.0, 0.0, 0.0}, .tex_coord = [_]f32 {0.0, 0.0} },
        .{ .pos = [_]f32 {-1.0, -1.0,  1.0}, .color = [_]f32 {0.0, 1.0, 0.0}, .tex_coord = [_]f32 {1.0, 0.0} },
        .{ .pos = [_]f32 {-1.0, -1.0, -1.0}, .color = [_]f32 {0.0, 0.0, 1.0}, .tex_coord = [_]f32 {1.0, 1.0} },
        .{ .pos = [_]f32 {-1.0,  1.0, -1.0}, .color = [_]f32 {0.0, 0.0, 1.0}, .tex_coord = [_]f32 {0.0, 1.0} },

        // x
        .{ .pos = [_]f32 { 1.0,  1.0, -1.0}, .color = [_]f32 {1.0, 0.0, 0.0}, .tex_coord = [_]f32 {1.0, 1.0} },
        .{ .pos = [_]f32 { 1.0, -1.0, -1.0}, .color = [_]f32 {0.0, 1.0, 0.0}, .tex_coord = [_]f32 {0.0, 1.0} },
        .{ .pos = [_]f32 { 1.0, -1.0,  1.0}, .color = [_]f32 {0.0, 0.0, 1.0}, .tex_coord = [_]f32 {0.0, 0.0} },
        .{ .pos = [_]f32 { 1.0,  1.0,  1.0}, .color = [_]f32 {0.0, 0.0, 1.0}, .tex_coord = [_]f32 {1.0, 0.0} },

        // -y
        .{ .pos = [_]f32 { 1.0, -1.0,  1.0}, .color = [_]f32 {1.0, 0.0, 0.0}, .tex_coord = [_]f32 {1.0, 1.0} },
        .{ .pos = [_]f32 { 1.0, -1.0, -1.0}, .color = [_]f32 {0.0, 1.0, 0.0}, .tex_coord = [_]f32 {0.0, 1.0} },
        .{ .pos = [_]f32 {-1.0, -1.0, -1.0}, .color = [_]f32 {0.0, 0.0, 1.0}, .tex_coord = [_]f32 {0.0, 0.0} },
        .{ .pos = [_]f32 {-1.0, -1.0,  1.0}, .color = [_]f32 {0.0, 0.0, 1.0}, .tex_coord = [_]f32 {1.0, 0.0} },

        // y
        .{ .pos = [_]f32 {-1.0,  1.0,  1.0}, .color = [_]f32 {1.0, 0.0, 0.0}, .tex_coord = [_]f32 {1.0, 1.0} },
        .{ .pos = [_]f32 {-1.0,  1.0, -1.0}, .color = [_]f32 {0.0, 1.0, 0.0}, .tex_coord = [_]f32 {0.0, 1.0} },
        .{ .pos = [_]f32 { 1.0,  1.0, -1.0}, .color = [_]f32 {0.0, 0.0, 1.0}, .tex_coord = [_]f32 {0.0, 0.0} },
        .{ .pos = [_]f32 { 1.0,  1.0,  1.0}, .color = [_]f32 {0.0, 0.0, 1.0}, .tex_coord = [_]f32 {1.0, 0.0} },
    });
    indices = try allocator.alloc(u32, 36);
    var i: u32 = 0;
    for(0..6) |j| {
        const base = @as(u32, @intCast(j * 4));
        for(0..2) |l| {
            for(0..3) |k| {
                indices[i] = base + (@as(u32, @intCast(k + l * 2)) % 4);
                i += 1;
            }
        }
    }
    //logger.debug("{}\n", .{std.json.fmt(indices, .{})});
}

fn load_model() !void {
    const scene = c.aiImportFile(utils.MODEL_PATH, c.aiProcess_ValidateDataStructure);
    const num_meshes = scene.*.mNumMeshes;

    const Context = struct {
        pub fn eql(ctx: @This(), a: VulkanEngine.Vertex, b: VulkanEngine.Vertex) bool {
            _ = ctx;
            return std.mem.eql(f32, &a.pos, &b.pos) and std.mem.eql(f32, &a.color, &b.color) and std.mem.eql(f32, &a.tex_coord, &b.tex_coord);
        }
        pub fn hash(ctx: @This(), key: VulkanEngine.Vertex) u64 {
            const vec2_hash_fn = std.hash_map.getAutoHashFn([2]u32, @This());
            const vec3_hash_fn = std.hash_map.getAutoHashFn([3]u32, @This());
            const pos = @as([3]u32, @bitCast(key.pos));
            const color = @as([3]u32, @bitCast(key.color));
            const tex_coord = @as([2]u32, @bitCast(key.tex_coord));
            return ((vec3_hash_fn(ctx, pos) ^ (vec3_hash_fn(ctx, color) << 1)) >> 1) ^ (vec2_hash_fn(ctx, tex_coord) << 1);
        }
    };

    const hashmap_type = std.HashMap(VulkanEngine.Vertex, u32, Context, std.hash_map.default_max_load_percentage);
    var unique_vertices = hashmap_type.init(allocator);

    for(0..num_meshes) |mesh_i| {
        // temporary
        std.debug.assert(mesh_i == 0);

        const mesh = scene.*.mMeshes[mesh_i];
        const num_vertices = mesh.*.mNumVertices;

        vertices = try allocator.alloc(VulkanEngine.Vertex, num_vertices);
        indices = try allocator.alloc(u32, num_vertices);

        const tex_coords = mesh.*.mTextureCoords[0];
        std.debug.assert(tex_coords != null);
        var last_i: u32 = 0;

        for(0..num_vertices) |v_i| {
            const vert = mesh.*.mVertices[v_i];
            const uv = tex_coords[v_i];

            const vertex = VulkanEngine.Vertex {
                .pos = [_]f32 { vert.x, vert.y, vert.z },
                .color = [_]f32 {1.0, 1.0, 1.0},
                .tex_coord = [_]f32 { uv.x, 1.0 - uv.y }
            };
            if(unique_vertices.get(vertex) == null) {
                try unique_vertices.put(vertex, last_i);
                vertices[last_i] = vertex;
                last_i += 1;
            }

            indices[v_i] = unique_vertices.get(vertex).?;
        }
        if(allocator.resize(vertices, last_i))
            vertices.len = last_i;
    }
    std.mem.reverse(u32, indices);
    logger.debug("vertices len: {}\n", .{vertices.len});
}

var device: *VulkanEngine.Device = undefined;
var command_pool: c.VkCommandPool = undefined;
var window: ?*c.GLFWwindow = undefined;
var render_resources: *VulkanEngine.RenderResources = undefined;
pub fn set_globals(_device: *VulkanEngine.Device, _command_pool: c.VkCommandPool, _window: ?*c.GLFWwindow, res: *VulkanEngine.RenderResources, sets: [utils.MAX_FRAMES_IN_FLIGHT]c.VkDescriptorSet) void {
    device = _device;
    command_pool = _command_pool;
    window = _window;
    render_resources = res;
    descriptor_sets = sets;
}


const MAT4_IDENTITY = c.mat4 {
    c.vec4 {1.0, 0.0, 0.0, 0.0},
    c.vec4 {0.0, 1.0, 0.0, 0.0},
    c.vec4 {0.0, 0.0, 1.0, 0.0},
    c.vec4 {0.0, 0.0, 0.0, 1.0}
};

fn _ptr(v: *const [3]f32) [*c]f32 {
    return @ptrCast(@constCast(v));
}



fn update_uniform_buffer(current_image: usize, uniform_buffers: *[utils.MAX_FRAMES_IN_FLIGHT]Buffer) void {
    const time: f32 = @as(f32, @floatFromInt(start_time.read() / 1000000)) / 10000.0;

    var model align(64) = MAT4_IDENTITY;
    const t: f32 = 0.5;
    c.glmc_scale(&model, _ptr(&c.vec3{t, t, t}));
    _ = time;

    const ubo: VulkanEngine.UniformBufferObject = .{
        .model = model,
        .view = camera.view,
        .proj = camera.perspective
    };
    uniform_buffers[current_image].allocation.write_data(&ubo, @sizeOf(VulkanEngine.UniformBufferObject), 0);

}


pub fn create_index_buffer() void {
    const buffer_size = @sizeOf(@TypeOf(indices[0])) * indices.len;

    var staging_buffer = Buffer.create_staging_buffer(buffer_size, indices);
    defer staging_buffer.deinit();
    
    index_buffer = BufferBuilder
        .builder(buffer_size)
        .with_usage(c.VK_BUFFER_USAGE_TRANSFER_DST_BIT | c.VK_BUFFER_USAGE_INDEX_BUFFER_BIT)
        .with_vma_usage(c.VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
        .with_vma_flags(c.VMA_ALLOCATION_CREATE_MAPPED_BIT | c.VMA_ALLOCATION_CREATE_HOST_ACCESS_RANDOM_BIT)
        .build();


    staging_buffer.copy_to(index_buffer, buffer_size);
}

pub fn create_instance_buffer() void {
    const buffer_size = @sizeOf(@TypeOf(instance_data[0])) * instance_data.len;

    var staging_buffer = Buffer.create_staging_buffer(buffer_size, &instance_data);
    defer staging_buffer.deinit();

    instance_buffer = BufferBuilder
        .builder(buffer_size)
        .with_vma_flags(c.VMA_ALLOCATION_CREATE_MAPPED_BIT | c.VMA_ALLOCATION_CREATE_HOST_ACCESS_RANDOM_BIT)
        .with_vma_usage(c.VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
        .with_usage(c.VK_BUFFER_USAGE_TRANSFER_DST_BIT | c.VK_BUFFER_USAGE_VERTEX_BUFFER_BIT)
        .build();

    staging_buffer.copy_to(instance_buffer, buffer_size);
}

pub fn create_vertex_buffer() void {
    const buffer_size = @sizeOf(@TypeOf(vertices[0])) * vertices.len;

    var staging_buffer = Buffer.create_staging_buffer(buffer_size, vertices);
    defer staging_buffer.deinit();

    vertex_buffer = BufferBuilder
        .builder(buffer_size)
        .with_usage(c.VK_BUFFER_USAGE_TRANSFER_DST_BIT | c.VK_BUFFER_USAGE_VERTEX_BUFFER_BIT)
        .with_vma_usage(c.VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
        .with_vma_flags(c.VMA_ALLOCATION_CREATE_MAPPED_BIT | c.VMA_ALLOCATION_CREATE_HOST_ACCESS_RANDOM_BIT)
        .build();
        

    staging_buffer.copy_to(vertex_buffer, buffer_size);
}


pub fn create_sync_objects() void {
    const semaphore_info: c.VkSemaphoreCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO
    };
    const fence_info: c.VkFenceCreateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
        .flags = c.VK_FENCE_CREATE_SIGNALED_BIT
    };

    for(0..utils.MAX_FRAMES_IN_FLIGHT) |i| {
        if(
            c.vkCreateSemaphore(device.get_handle(), &semaphore_info, VulkanEngine.api_allocator, &image_available_semas[i]) != c.VK_SUCCESS or
            c.vkCreateSemaphore(device.get_handle(), &semaphore_info, VulkanEngine.api_allocator, &render_finished_semas[i]) != c.VK_SUCCESS or
            c.vkCreateFence(device.get_handle(), &fence_info, VulkanEngine.api_allocator, &in_flight_fences[i]) != c.VK_SUCCESS
        ) {
            std.debug.panic("failed to create semaphores", .{});
        }
    }
}

var done = [_]bool {false} ** utils.MAX_FRAMES_IN_FLIGHT;

fn draw_frame(uniform_buffers: *[utils.MAX_FRAMES_IN_FLIGHT]Buffer) !void {
    const t = tracy.trace(@src());
    const frame = tracy.frame(null);
    defer t.end();
    defer frame.end();

    check(c.vkWaitForFences(device.get_handle(), 1, &in_flight_fences[current_frame], c.VK_TRUE, std.math.maxInt(u64)));
    if(done[current_frame]) {
        tracy.frameMarkEnd(fences_name[current_frame]);
    } else {
        done[current_frame] = true;
    }

    var image_index: u32 = undefined;
    var result = c.vkAcquireNextImageKHR(device.get_handle(), render_resources.swap_chain.get_handle(), std.math.maxInt(u64), image_available_semas[current_frame], null, &image_index);

    if(result == c.VK_ERROR_OUT_OF_DATE_KHR) {
        //try recreate_swap_chain();
        try VulkanEngine.recreate_render_resources(render_resources);
        return;
    } else if(result != c.VK_SUCCESS and result != c.VK_SUBOPTIMAL_KHR) {
        std.debug.panic("failed to acquire swap chain image", .{});
    }

    update_uniform_buffer(current_frame, uniform_buffers);

    check(c.vkResetFences(device.get_handle(), 1, &in_flight_fences[current_frame]));

    check(c.vkResetCommandBuffer(command_buffers[current_frame], 0));
    record_command_buffer(command_buffers[current_frame], image_index);


    const wait_semaphores = [_]c.VkSemaphore { image_available_semas[current_frame] };
    const wait_stages = [_]c.VkPipelineStageFlags { c.VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
    const signal_semaphores = [_]c.VkSemaphore { render_finished_semas[current_frame] };
    const submit_info: c.VkSubmitInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .waitSemaphoreCount = wait_semaphores.len,
        .pWaitSemaphores = &wait_semaphores,
        .pWaitDstStageMask = &wait_stages,
        .commandBufferCount = 1,
        .pCommandBuffers = &command_buffers[current_frame],
        .signalSemaphoreCount = signal_semaphores.len,
        .pSignalSemaphores = &signal_semaphores
    };

    tracy.frameMarkStart(fences_name[current_frame]);
    check2(c.vkQueueSubmit(device.graphics_queue.?, 1, &submit_info, in_flight_fences[current_frame]), "failed to submit draw command buffer"); 

    const swap_chains = [_]c.VkSwapchainKHR { render_resources.swap_chain.get_handle() };
    const present_info: c.VkPresentInfoKHR = .{
        .sType = c.VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = &signal_semaphores,
        .swapchainCount = swap_chains.len,
        .pSwapchains = &swap_chains,
        .pImageIndices = &image_index
    };

    result = c.vkQueuePresentKHR(device.present_queue.?, &present_info);

    if(result == c.VK_ERROR_OUT_OF_DATE_KHR or result == c.VK_SUBOPTIMAL_KHR or frame_buffer_resized) {
        frame_buffer_resized = false;
        //try recreate_swap_chain();
        try VulkanEngine.recreate_render_resources(render_resources);
    } else if(result != c.VK_SUCCESS) {
        std.debug.panic("failed to present swap chain image", .{});
    }
    current_frame = (current_frame + 1) % utils.MAX_FRAMES_IN_FLIGHT;
}

pub fn create_command_buffers() void {
    const alloc_info: c.VkCommandBufferAllocateInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .commandPool = command_pool,
        .level = c.VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = utils.MAX_FRAMES_IN_FLIGHT
    };

    check(c.vkAllocateCommandBuffers(device.get_handle(), &alloc_info, &command_buffers));
    for(0..utils.MAX_FRAMES_IN_FLIGHT) |i| {
        // vk_ctx[i] = tracy.VkCtx.init(device.gpu, device.get_handle(), graphics_queue, command_buffers[i]);
        fences_name[i] = std.fmt.allocPrintZ(allocator, "Fence {}", .{i}) catch unreachable;
    }
}

const floor = true;

const instance_data = if(floor)
blk: {
    @setEvalBranchQuota(10000);
    const r: i32 = 5;
    var data: [(2*r+1)*(2*r+1)]VulkanEngine.InstanceData = undefined;
    for(0..2*r + 1) |x| {
        for(0..2*r + 1) |y| {
            data[x * (2 * r + 1) + y] = .{
                .pos = [_]f32 {
                    @floatFromInt((@as(i32, @intCast(x)) - r) * 2),
                    @floatFromInt((@as(i32, @intCast(y)) - r) * 2),
                    1.0
                }
            };
        }
    }
    break :blk data;
} else 
[_]VulkanEngine.InstanceData {
    .{ .pos = [_]f32 {1.0, 0.0, -1.0} },
    .{ .pos = [_]f32 {-1.0, 0.0, -1.0} },
    .{ .pos = [_]f32 {1.0, 0.0, 1.0} },
    .{ .pos = [_]f32 {-1.0, 0.0, 1.0} }
};

// note: tutorial pass the command_buffer as an arg
fn record_command_buffer(command_buffer: c.VkCommandBuffer, image_index: u32) void {
    const begin_info: c.VkCommandBufferBeginInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
    };

    check(c.vkBeginCommandBuffer(command_buffer, &begin_info));

    const zone = vk_ctx[current_frame].zone(@src(), command_buffer, null);

    const clear_values = [_]c.VkClearValue {
        .{
            .color = .{
                .float32 = [_]f32 {0.0, 0.0, 0.0, 1.0}
            }
        },
        .{
            .depthStencil = .{ .depth = 1.0, .stencil = 0 }
        }
    };
    const swap_extent = render_resources.swap_chain.extent.?;
    const render_pass_info: c.VkRenderPassBeginInfo = .{
        .sType = c.VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
        .renderPass = render_resources.render_pass.?,
        .framebuffer = render_resources.framebuffers[image_index],
        .renderArea = .{
            .offset = .{ .x = 0, .y = 0 },
            .extent = .{ .width = swap_extent.width, .height = swap_extent.height }
        },
        .clearValueCount = clear_values.len,
        .pClearValues = &clear_values
    };

    c.vkCmdBeginRenderPass(command_buffer, &render_pass_info, c.VK_SUBPASS_CONTENTS_INLINE);
        c.vkCmdBindPipeline(command_buffer, c.VK_PIPELINE_BIND_POINT_GRAPHICS, render_resources.graphics_pipeline);

        const viewport: c.VkViewport = .{
            .x = 0.0,
            .y = 0.0,
            .width = @floatFromInt(swap_extent.width),
            .height = @floatFromInt(swap_extent.height),
            .minDepth = 0.0,
            .maxDepth = 1.0
        };
        c.vkCmdSetViewport(command_buffer, 0, 1, &viewport);

        const scissor: c.VkRect2D = .{
            .offset = .{ .x = 0, .y = 0},
            .extent = .{ .width = swap_extent.width, .height = swap_extent.height }
        };
        c.vkCmdSetScissor(command_buffer, 0, 1, &scissor);

        const vertex_buffers = [_]c.VkBuffer { vertex_buffer.get_handle(), instance_buffer.get_handle() };
        const offsets = [_]c.VkDeviceSize { 0, 0 };
        c.vkCmdBindVertexBuffers(command_buffer, 0, vertex_buffers.len, &vertex_buffers, &offsets);

        c.vkCmdBindIndexBuffer(command_buffer, index_buffer.get_handle(), 0, c.VK_INDEX_TYPE_UINT32);

        c.vkCmdBindDescriptorSets(command_buffer, c.VK_PIPELINE_BIND_POINT_GRAPHICS, render_resources.graphics_pipeline_layout, 0, 1, &descriptor_sets[current_frame], 0, null);

        c.vkCmdDrawIndexed(command_buffer, @as(u32, @intCast(indices.len)), instance_data.len, 0, 0, 0);
        //c.vkCmdDrawIndexed(command_buffer, @as(u32, @intCast(indices.len)), 1, 0, 0, 0);
    c.vkCmdEndRenderPass(command_buffer);

    vk_ctx[current_frame].collect(command_buffer);
    zone.end();
    check(c.vkEndCommandBuffer(command_buffer));
}



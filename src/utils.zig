const std = @import("std");
const c = @import("c.zig");

const Level = std.log.Level;

const reset = "\u{001b}[m";
const bold = "\u{001b}[1m";
const dark = "\u{001b}[2m";
const underline = "\u{001b}[4m";
const blink = "\u{001b}[5m";
const reverse = "\u{001b}[7m";
const concealed = "\u{001b}[8m";
const clear_line = "\u{001b}[K";

// Foreground colors
const black = "\u{001b}[30m";
const red = "\u{001b}[31m";
const green = "\u{001b}[32m";
const yellow = "\u{001b}[33m";
const blue = "\u{001b}[34m";
const magenta = "\u{001b}[35m";
const cyan = "\u{001b}[36m";
const white = "\u{001b}[37m";

const bright_black = "\u{001b}[90m";
/// Background colors
const on_black = "\u{001b}[40m";
const on_red = "\u{001b}[41m";
const on_green = "\u{001b}[42m";
const on_yellow = "\u{001b}[43m";
const on_blue = "\u{001b}[44m";
const on_magenta = "\u{001b}[45m";
const on_cyan = "\u{001b}[46m";
const on_white = "\u{001b}[47m";

/// Bold colors
const yellow_bold = "\u{001b}[33m\u{001b}[1m";
const red_bold = "\u{001b}[31m\u{001b}[1m";
const bold_on_red = "\u{001b}[1m\u{001b}[41m";


fn get_level_color(comptime level: Level) []const u8 {
    return switch(level) {
        .debug => cyan,
        .info => green,
        .warn => yellow_bold,
        .err => red_bold
    };
}

fn log_fn(
    comptime level: Level,
    comptime scope: @Type(.EnumLiteral),
    comptime format: []const u8,
    args: anytype,
) void {
    const color = switch(level) {
        .debug => cyan,
        .info => green,
        .warn => yellow_bold,
        .err => red_bold
    };
    const level_txt = "[" ++ color ++ comptime level.asText() ++ reset ++ "] ";
    const prefix = if (scope == .default) "" else "(" ++ bright_black ++ @tagName(scope) ++ reset ++ ") ";

    const stderr = std.io.getStdErr().writer();
    var bw = std.io.bufferedWriter(stderr);
    const writer = bw.writer();

    std.debug.lockStdErr();
    defer std.debug.unlockStdErr();
    nosuspend {
        writer.print(level_txt ++ prefix ++ format ++ "\n", args) catch return;
        bw.flush() catch return;
    }
}

pub fn Logger(comptime scope: @Type(.EnumLiteral)) type {
    return std.log.scoped(scope);
}


fn get_prop_values_ty(comptime fn_T: type) type {
    const ty = switch(@typeInfo(fn_T)) {
        .Fn => |fn_t| fn_t,
        else => unreachable
    };
    const values_ty = switch(@typeInfo(ty.params[ty.params.len - 1].type.?)) {
        .Pointer => |pt| pt,
        else => unreachable
    }.child;

    return values_ty;
}

pub fn Scoped(logger: type) type {
    return struct {
        pub fn vk_get_properties(allocator: std.mem.Allocator, comptime func: anytype, arguments: anytype) ![]get_prop_values_ty(@TypeOf(func)) {
            const fn_T = @TypeOf(func);
            const ty = switch(@typeInfo(fn_T)) {
                .Fn => |fn_t| fn_t,
                else => unreachable
            };
            const p_len = ty.params.len;
            const array_type = ty.params[ty.params.len - 1].type.?;

            var count: u32 = 0;
            const args = switch(p_len) {
                2 => .{&count, @as(array_type, null)},
                3 => .{arguments, &count, @as(array_type, null)},
                4 => .{arguments[0], arguments[1], &count, @as(array_type, null)},
                else => unreachable
            };
            const returns_void = ty.return_type.? == void;

            if(returns_void)
                @call(.auto, func, args)
            else
                check(@call(.auto, func, args));

            const values = try allocator.alloc(get_prop_values_ty(fn_T), count);

            // can't edit the first tuple
            const args_2 = switch(p_len) {
                2 => .{&count, @as(array_type, @ptrCast(values))},
                3 => .{arguments, &count, @as(array_type, @ptrCast(values))},
                4 => .{arguments[0], arguments[1], &count, @as(array_type, @ptrCast(values))},
                else => unreachable
            };
            if(returns_void)
                @call(.auto, func, args_2)
            else
                check(@call(.auto, func, args_2));
            return values;
        }

        pub fn check2(value: c.VkResult, comptime message: []const u8) void {
            if(value != c.VK_SUCCESS) {
                logger.err(message ++ " (received error {s} ({}))", .{error_to_str(value), value});
                std.process.exit(1);
            }
        }

        pub fn check(value: c.VkResult) void {
            if(value != c.VK_SUCCESS) {
                logger.err("Detected vulkan error: {s} ({})", .{error_to_str(value), value});
                std.process.exit(1);
            }
        }
    };
}

pub fn error_to_str(err: c.VkResult) []const u8 {
    return switch(err) {
        c.VK_NOT_READY => "NOT_READY",
        c.VK_TIMEOUT => "TIMEOUT",
        c.VK_EVENT_SET => "EVENT_SET",
        c.VK_EVENT_RESET => "EVENT_RESET",
        c.VK_INCOMPLETE => "INCOMPLETE",
        c.VK_ERROR_OUT_OF_HOST_MEMORY => "ERROR_OUT_OF_HOST_MEMORY",
        c.VK_ERROR_OUT_OF_DEVICE_MEMORY => "ERROR_OUT_OF_DEVICE_MEMORY",
        c.VK_ERROR_INITIALIZATION_FAILED => "ERROR_INITIALIZATION_FAILED",
        c.VK_ERROR_DEVICE_LOST => "ERROR_DEVICE_LOST",
        c.VK_ERROR_MEMORY_MAP_FAILED => "ERROR_MEMORY_MAP_FAILED",
        c.VK_ERROR_LAYER_NOT_PRESENT => "ERROR_LAYER_NOT_PRESENT",
        c.VK_ERROR_EXTENSION_NOT_PRESENT => "ERROR_EXTENSION_NOT_PRESENT",
        c.VK_ERROR_FEATURE_NOT_PRESENT => "ERROR_FEATURE_NOT_PRESENT",
        c.VK_ERROR_INCOMPATIBLE_DRIVER => "ERROR_INCOMPATIBLE_DRIVER",
        c.VK_ERROR_TOO_MANY_OBJECTS => "ERROR_TOO_MANY_OBJECTS",
        c.VK_ERROR_FORMAT_NOT_SUPPORTED => "ERROR_FORMAT_NOT_SUPPORTED",
        c.VK_ERROR_SURFACE_LOST_KHR => "ERROR_SURFACE_LOST_KHR",
        c.VK_ERROR_NATIVE_WINDOW_IN_USE_KHR => "ERROR_NATIVE_WINDOW_IN_USE_KHR",
        c.VK_SUBOPTIMAL_KHR => "SUBOPTIMAL_KHR",
        c.VK_ERROR_OUT_OF_DATE_KHR => "ERROR_OUT_OF_DATE_KHR",
        c.VK_ERROR_INCOMPATIBLE_DISPLAY_KHR => "ERROR_INCOMPATIBLE_DISPLAY_KHR",
        c.VK_ERROR_VALIDATION_FAILED_EXT => "ERROR_VALIDATION_FAILED_EXT",
        c.VK_ERROR_INVALID_SHADER_NV => "ERROR_INVALID_SHADER_NV",
        else => "UNKNOWN_ERROR"
    };
}

pub const CStrArray = [*c]const [*c]const u8;
pub const CStr = [*c]const u8;


pub const std_options: std.Options = .{
    .logFn = log_fn,
    .log_level = .debug
};


pub const MODEL_PATH = "src/models/viking_room.obj";
pub const TEXTURE_PATH = "src/textures/uvmap.jpg";
//pub const TEXTURE_PATH = "src/textures/viking_room.png";
pub const ENABLE_VALIDATION_LAYERS = true;
pub const MAX_FRAMES_IN_FLIGHT = 2;

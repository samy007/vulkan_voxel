pub usingnamespace @cImport({
    @cDefine("GLFW_INCLUDE_VULKAN", {});
    @cInclude("GLFW/glfw3.h");
    @cDefine("CGLM_FORCE_DEPTH_ZERO_TO_ONE", {});
    @cInclude("cglm/call.h");
    @cInclude("time.h");
    //@cDefine("STB_IMAGE_IMPLEMENTATION", {});
    @cInclude("stb/stb_image.h");

    @cInclude("assimp/cimport.h");
    @cInclude("assimp/scene.h");
    @cInclude("assimp/postprocess.h");

    @cInclude("third_party/VulkanMemoryAllocator/include/vk_mem_alloc.h");
});

#include <Tracy/common/TracyAlloc.hpp>
#include <cstdio>
#include <vulkan/vulkan.h>

#define TRACY_ENABLE
#include <Tracy/tracy/TracyVulkan.hpp>

extern "C" {
	void* __tracy_create_context(VkPhysicalDevice physdev, VkDevice device, VkQueue queue, VkCommandBuffer cmdbuf) {
		return TracyVkContext(physdev, device, queue, cmdbuf);
	}

	void __tracy_destroy_context(void* ctx) {
		TracyVkDestroy((TracyVkCtx) ctx);
	}
	
	void __tracy_collect_context(void* ctx, VkCommandBuffer cmdbuf) {
		TracyVkCollect(((TracyVkCtx) ctx), cmdbuf);
	}

	void* __tracy_create_vk_zone(void* ctx, const tracy::SourceLocationData* srcloc, VkCommandBuffer cmdbuf, bool is_active) {
		auto scope = (tracy::VkCtxScope*) tracy::tracy_malloc(sizeof(tracy::VkCtxScope));
		new (scope) tracy::VkCtxScope((TracyVkCtx) ctx, srcloc, cmdbuf, is_active);
		return scope;
	}

	void __tracy_end_vk_zone(void* zone) {
		delete (tracy::VkCtxScope*) zone;
	}
}
